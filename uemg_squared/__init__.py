# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import shutil

import numpy

"""uEMG Uterine EMG simulator.

:Contributors:
    J. Laforet,  M. Yochum

**Dependencies:**

:Requiered:
    h5py, astropy, jsonschema, psutil, scipy, numpy

:Optionnal:
    matplotlib, pyqtgraph, moviepy, progressbar, py-cpuinfo

"""

import os
import time
import h5py
import astropy.units as aunits
import logging
import json
import glob
import jsonschema
from pkg_resources import resource_filename
from . import reporting
from . import graphics
from . import cellular
from . import tissular
from . import volume_conductor
from . import elec_grids

try:
    from moviepy.editor import ImageSequenceClip

    _hasmoviepy = True
except ImportError:
    _hasmoviepy = False
except RuntimeError:
    _hasmoviepy = False
logger = logging.getLogger(__name__)


def check_params(parameters):
    """
    Check parameters for certain constraints based on name or units (eg. durations must be positive).
    :param parameters: parameters json dictionary
    """
    for par_list in get_all(parameters, 'parameters'):
        for par in par_list:
            if type(par["value"]) is str:
                continue
            elif par["name"] == "centerx" or par["name"] == "rotation" or par["name"] == "centery":
                continue
            elif par["unit"] == "vector":
                continue
            elif par["name"].rstrip("xyz") == "nb_cell_":
                if par["value"] <= 0:
                    raise ValueError(
                        "Value of parameter {} is incorrect, cell number must be positive".format(par["name"]))

            elif aunits.Unit(par["unit"]).is_equivalent("m"):
                if par["value"] < 0:
                    raise ValueError("Value of parameter {} is incorrect, length must be positive".format(par["name"]))

            elif aunits.Unit(par["unit"]).is_equivalent("s"):
                if par["value"] < 0:
                    raise ValueError("Value of parameter {} is incorrect, time must be positive".format(par["name"]))

            elif aunits.Unit(par["unit"]).is_equivalent("S/m"):
                if par["value"] <= 0:
                    raise ValueError(
                        "Value of parameter {} is incorrect, conductivity must be positive".format(par["name"]))

            elif aunits.Unit(par["unit"]).is_equivalent("Hz"):
                if par["value"] <= 0:
                    raise ValueError(
                        "Value of parameter {} is incorrect, frequency must be positive".format(par["name"]))


def get_all(myjson, key):
    """
    Yeild all *key* in the input json
    :param myjson: Json input object
    :param key: key to return
    :return: dict
    """
    if type(myjson) is dict:
        for jsonkey in myjson:
            if jsonkey == key:
                yield myjson[jsonkey]
            elif type(myjson[jsonkey]) in (list, dict):
                for d in get_all(myjson[jsonkey], key):
                    yield d
    elif type(myjson) is list:
        for item in myjson:
            if type(item) in (list, dict):
                for d in get_all(item, key):
                    yield d


class Manager(object):
    """
    Simulation manager class
    """

    def __init__(self, jsonfile, basedir=None, subdir=None):
        """
        Simulation manager class

        :param jsonfile: Input json file describing the simulation
        :param basedir: working directory, if *None* then *pwd* is used.
        :return:

        """
        t0 = time.time()
        self.curr_time = time.localtime()
        if not subdir:
            self.subdir = 'uEMG_' + \
                      time.strftime("%Y%m%d_%H%M%S", self.curr_time) + '/'
        else:
            self.subdir = subdir
        if basedir is not None:
            self.subdir = os.path.join(basedir, self.subdir)
        if not (os.path.isdir(self.subdir)):
            os.mkdir(self.subdir)

        handler = logging.FileHandler(os.path.join(self.subdir, self.__module__ + '.log'))
        handler.setFormatter(logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'))
        logger.addHandler(handler)
        logger.setLevel(level=logging.INFO)

        self.JSONfile = jsonfile
        # copy(self.JSONfile, self.subdir)

        with open(resource_filename(self.__module__.split('.')[0], '/data/UnitsDictionary.json'), 'r') as f_units:
            self.units = json.load(f_units)
        if isinstance(self.JSONfile, dict):
            self.parameters = self.JSONfile
        else:
            with open(self.JSONfile, 'r') as f_params:
                self.parameters = json.load(f_params)

        with open(resource_filename(self.__module__.split('.')[0], '/data/schema_muscle.json'), 'r') as f_schema:
            jsonschema.validate(self.parameters, json.load(f_schema))

        check_params(self.parameters)
        # self.apply_units()

        self.comptime = dict()
        # self.outfiles = [self.JSONfile]
        self.graphfiles = []
        self.params_calc = []
        self.purpose = self.parameters['Purpose']
        self.username = None
        self.code_version = 0.1
        self.cell_elec_mdl = None
        self.cell_meca_mdl = None
        self.tissue = None
        self.tissue_mdl = None
        self.itype = None
        self.vc = None
        self.vc_mdl = None

        self.electGrids = None
        self.set_models()
        self.comptime['Initialization:'] = time.time() - t0

    def apply_units(self, ):
        """
        Apply units to all parameters defined in the json, does conversions when needed.
        :return:
        """
        for par_list in get_all(self.parameters, 'parameters'):
            for par in par_list:
                if not par["unit"] == 'vector':
                    try:
                        aunits.Unit(self.units[par["name"]])
                    except ValueError as Err:
                        logger.exception(Err)
                    else:
                        par["value"] *= aunits.Unit(par["unit"])
                        par["value"].to(self.units[par["name"]])

    def set_models(self, ):

        """
        Initialise the models.

        """

        try:
            self.tissue_mdl = [mdl for mdl in self.parameters['Muscle']['Models']
                               if mdl['class'].split('.')[0] == 'tissular'][0]
        except IndexError:
            print("ERROR TISSUE")
        else:
            paramT = {par['name']: par['value'] for par in self.tissue_mdl['parameters']}
            self.tissue = eval(self.tissue_mdl['class'] + "(paramT)")
            self.params_calc.append(self.tissue)

        try:
            self.cell_elec_mdl = [mdl for mdl in self.parameters['Muscle']['Models']
                                  if (
                                      mdl['class'].split('.')[0] == 'cellular' and mdl['class'].split('.')[
                                          1].startswith(
                                          'Red'))][0]
        except IndexError as Err:
            print('No electrical model defined')
            raise Err
        else:
            paramE = {par['name']: par['value'] for par in self.tissue_mdl['parameters']}
            self.tissue.mdl_elec = eval(self.cell_elec_mdl['class'] + "(self.tissue.shape,paramE)")
            self.params_calc.append(self.tissue.mdl_elec)

        try:
            self.cell_meca_mdl = [mdl for mdl in self.parameters['Muscle']['Models']
                                  if (
                                      mdl['class'].split('.')[0] == 'cellular' and mdl['class'].split('.')[
                                          1].startswith(
                                          'Hai'))][0]
        except IndexError:
            print('No force model defined')
        else:
            paramF = {par['name']: par['value'] for par in self.tissue_mdl['parameters']}
            self.tissue.mdl_force = eval(self.cell_meca_mdl['class'] + "(self.tissue.shape,paramF)")
            self.params_calc.append(self.tissue.mdl_force)

        try:
            self.vc_mdl = [mdl for mdl in self.parameters['Muscle']['Models']
                               if mdl['class'].split('.')[0] == 'volume_conductor'][0]
        except IndexError:
            print("ERROR VC")
        else:
            paramV = {par['name']: par['value'] for par in self.vc_mdl['parameters']}
            paramV['shape'] = self.tissue.shape
            paramV['cellsize'] = (self.tissue.cellsize_x, self.tissue.cellsize_y) #Valid ONLY FOR 2D!
            self.vc = eval(self.vc_mdl['class'] + "(paramV)")
            self.params_calc.append(self.vc)

    def make_emg(self):

        nbGrid = len([mdl for mdl in self.parameters['Muscle']['Models']
                               if mdl['class'].split('.')[0] == 'elec_grids'])

        #determine total number of electrode grid
        #for par_list in get_all(self.parameters['Muscle']['Models'], "model"):
        #    for parl in par_list:
        #        if parl['class'].startswith('elec_grids'):
        #            for par in get_all(parl, "parameters"):
        #                nbGrid += 1
        if nbGrid > 0:
            self.electGrids = []
            grids = [mdl for mdl in self.parameters['Muscle']['Models']
                               if mdl['class'].split('.')[0] == 'elec_grids']
            for idx in numpy.arange(nbGrid):
                parl = grids[idx]
                if parl['class'].startswith("elec_grids.Grid"):
                    center = numpy.zeros(2)
                    delta = numpy.zeros(2)
                    nelec = numpy.zeros(2)
                    rotation = 0
                    for dico in get_all(parl, "parameters"):
                        for par in dico:
                            if par['name'] == 'center_x':
                                center[0] = numpy.round(par['value']/self.tissue.cellsize_x)
                            if par['name'] == 'center_y':
                                center[1] = numpy.round(par['value']/self.tissue.cellsize_y)
                            if par['name'] == "delta_x":
                                delta[0] = numpy.round(par['value']/self.tissue.cellsize_x)
                            if par['name'] == 'delta_y':
                                delta[1] = numpy.round(par['value']/self.tissue.cellsize_y)
                            if par['name'] == "NsigX":
                                nelec[0] = int(par['value'])
                            if par['name'] == "NsigY":
                                nelec[1] = int(par['value'])
                            if par['name'] == "rotation":
                                rotation = par['value']

                            if parl['class'].endswith("Oval") and par['name'] == "eray":
                                eray = numpy.zeros((2, 2))
                                eray[0][0] = 1./self.tissue.cellsize_x
                                eray[0][1] = 1./self.tissue.cellsize_x
                                eray[1][0] = 2./self.tissue.cellsize_y
                                eray[1][1] = 2./self.tissue.cellsize_y
                            elif parl['class'].endswith("Concent") and par['name'] == "eray":
                                eray = numpy.zeros((3, 2))
                                eray[0][0] = 1./self.tissue.cellsize_x
                                eray[0][1] = 1./self.tissue.cellsize_y
                                eray[1][0] = 2./self.tissue.cellsize_x
                                eray[1][1] = 2./self.tissue.cellsize_y
                                eray[2][0] = 3./self.tissue.cellsize_x
                                eray[2][1] = 3./self.tissue.cellsize_y
                            elif parl['class'].endswith("Circ") and par['name'] == "eray":
                                eray = numpy.zeros(2)
                                eray[0] = numpy.round(par['value']/self.tissue.cellsize_x)
                                eray[1] = numpy.round(par['value']/self.tissue.cellsize_y)

                    if parl['class'].endswith("Oval"):
                        eg = elec_grids.GridOval(numpy.swapaxes(self.vc.skinpot,0,2), center, delta, rotation, eray, nelec)
                        self.electGrids.append(eg)
                        self.params_calc.append(eg)
                    elif parl['class'].endswith("Concent"):
                        eg = elec_grids.GridConcent(numpy.swapaxes(self.vc.skinpot,0,2), center, delta, rotation, eray, nelec)
                        self.electGrids.append(eg)
                        self.params_calc.append(eg)
                    elif parl['class'].endswith("Circ"):
                        eg = elec_grids.GridCirc(numpy.swapaxes(self.vc.skinpot,0,2), center, delta, rotation, eray, nelec)
                        self.electGrids.append(eg)
                        self.params_calc.append(eg)

    def compute(self, ):
        """
        Simulate the models.

        """

        try:
            t0 = time.time()
            self.tissue.compute()
            self.comptime['Tissue simulation'] = time.time() - t0
        except AttributeError:
            logger.exception("No integration defined.")
        except KeyError:
            logger.exception("Wrong parameter for integration")

        try:
            t0 = time.time()
            self.vc.compute(self.tissue.Vm)
            self.comptime['Volume conductor simulation'] = time.time() - t0
        except AttributeError:
            pass
        except KeyError:
            logger.exception("Wrong parameter for volume conductor")
        else:
            t0 = time.time()
            self.make_emg()
            for eg in self.electGrids:
                eg.apply()
            self.comptime['Electrod grids simulation'] = time.time() - t0

    def save_hdf5(self, fname):
        """
        Save a hdf5 file with the results of all provided models.

        :param fname:
        :fname: Filename to be used, extension is automatically added.
        """
        tmdl = self.tissue
        vc = self.vc
        eg = self.electGrids
        with h5py.File(fname + ".hdf5", "w") as f:
            if tmdl is not None:
                f.create_dataset("time", data=tmdl.tsave)

                gm = f.create_group("myometrium")
                # gm.create_dataset("time", data=tmdl.tsave)
                for var in ['Vm', 'Force', 'Positions', 'Stretches']:
                    try:
                        gm.create_dataset(var, data=tmdl.__dict__[var])
                    except KeyError:
                        logger.warning("No variable {} to save in this simulation.".format(var))
                gm.attrs["nt"] = tmdl.tmax / tmdl.f_sample
                gm.attrs["tmax"] = tmdl.tmax
                gm.attrs["f_sample"] = tmdl.f_sample

                if tmdl.cellsize_x is not None:
                    gm.attrs["cellsize_x"] = tmdl.cellsize_x
                if tmdl.cellsize_y is not None:
                    gm.attrs["cellsize_y"] = tmdl.cellsize_y
                if tmdl.cellsize_z is not None:
                    gm.attrs["cellsize_z"] = tmdl.cellsize_z

                gm.attrs["shape"] = tmdl.Vm.shape[1:]

                logger.info("Myometrium level results saved.")

            if vc is not None:
                gv = f.create_group("Skin")
                gv.create_dataset('Potential', data=vc.skinpot)
                for param in vc.__dict__ .keys():
                    if param.startswith("thick"):
                        gv.attrs[param] = vc.__dict__[param]
                gv.attrs["cellsizes"] = vc.cellsize
                logger.info("Surface level results saved.")

            if eg:
                ge = f.create_group("EMG")
                for idx, electrodeGrid in enumerate(self.electGrids):
                    gs = ge.create_group("Grid_{}".format(idx+1))
                    gs.attrs["centers"] = (electrodeGrid.centers_x, electrodeGrid.centers_y)
                    gs.attrs["tissue_shape"] = electrodeGrid.gain_idx[0].shape
                    gs.create_dataset("Monopolar", data=electrodeGrid.sigs)
                    gs.create_dataset("Elec_gains", data=electrodeGrid.gain_idx)
                    logger.info("EMG signals saved for electrode grid {}.".format(idx+1))

    def save_emg(self, fname):
        """
        Save a hdf5 file with only the EMG generated.

        :param fname:
        :fname: Filename to be used, extension is automatically added.
        """

        eg = self.electGrids
        with h5py.File(fname + ".hdf5", "w") as f:

            f.create_dataset("time", data=self.tissue.tsave)

            if eg:
                ge = f.create_group("EMG")
                for idx, electrodeGrid in enumerate(self.electGrids):
                    gs = ge.create_group("Grid_{}".format(idx+1))
                    gs.attrs["centers"] = (electrodeGrid.centers_x, electrodeGrid.centers_y)
                    gs.attrs["tissue_shape"] = electrodeGrid.gain_idx[0].shape
                    gs.create_dataset("Monopolar", data=electrodeGrid.sigs)
                    gs.create_dataset("Elec_gains", data=electrodeGrid.gain_idx)
                    logger.info("EMG signals saved for electrode grid {}.".format(idx+1))

    def finish(self, batch=False):
        """
        Closure function:

        * Save the json input
        * Save the results in hdf5 format
        * Generate base graphics
        * Generate html report of the simulation

        :param batch: if *True* only EMG signals will be saved, surfaces will be discarded.
        """

        t0 = time.time()
        with open(os.path.join(self.subdir, 'Simulation_config.json'), 'w') as f_config:
            json.dump(self.parameters, f_config, indent=4, sort_keys=True)
        if batch:
            self.save_emg(os.path.join(self.subdir, 'EMG' + time.strftime("_%Y%m%d_%H%M%S", self.curr_time)))
        else:
            self.save_hdf5(os.path.join(self.subdir, 'Results' + time.strftime("_%Y%m%d_%H%M%S", self.curr_time)))
            variables = {'Vm', 'Force', 'Stretches'} & set(self.tissue.__dict__.keys())
            if (len(self.tissue.shape) == 1) and (self.tissue.shape == 1):
                for var in variables:
                    ifile = graphics.plot_line(self.tissue.__dict__[var], var, self, export=True, display=False)
                    self.graphfiles.append(ifile)
            elif len(self.tissue.shape) == 1:
                for var in variables:
                    ifile = graphics.plot_surf_pg(self.tissue.__dict__[var], var, self, export=True, display=False)
                    self.graphfiles.append(ifile)
            elif len(self.tissue.shape) == 2 and _hasmoviepy:
                for var in variables:
                    vidname = "myometrium_" + var + time.strftime("_%Y%m%d_%H%M%S", self.curr_time) + '.mp4'
                    self.graphfiles.append(graphics.vid_surf(self.tissue.__dict__[var], name=vidname, manager=self))
                    # if not (os.path.isdir(os.path.join(self.subdir, 'tmppng'))):
                    #     os.mkdir(os.path.join(self.subdir, 'tmppng'))
                    # graphics.plot_surf_pg(self.tissue.__dict__[var], var, self, export=True, display=False)
                    # try:
                    #     list_png = sorted(
                    #         [str(pngfile) for pngfile in glob.glob(os.path.join(self.subdir, 'tmppng', '*.png'))])
                    #     # str(pngfile) to get non-unicode strings
                    #     clip = ImageSequenceClip(list_png, fps=min(self.tissue.f_sample, 25))
                    #     clip = clip.resize(400. / clip.size[0])
                    #
                    #     animation_name = os.path.join(self.subdir,
                    #                                   "myometrium_" + var + time.strftime("_%Y%m%d_%H%M%S", self.curr_time))
                    #     # clip.write_gif(animation_name+'.gif',fps=min(self.tissue.f_sample,25))
                    #     # self.graphfiles.append(os.path.split(animation_name)[1]+'.gif')
                    #     clip.write_videofile(animation_name + '.webm', codec='libvpx')
                    #     self.graphfiles.append(os.path.split(animation_name)[1] + '.webm')
                    # except OSError as Err:
                    #     raise Err
                    # else:
                    #     shutil.rmtree(os.path.join(self.subdir, 'tmppng'))
            elif len(self.tissue.shape) == 3 and _hasmoviepy:
                for var in variables:
                    if not (os.path.isdir(os.path.join(self.subdir, 'tmppng'))):
                        os.mkdir(os.path.join(self.subdir, 'tmppng'))
                    graphics.plot_surf_pg(self.tissue.__dict__[var], var.mean(axis=numpy.argmin(self.tissue.shape) + 1),
                                          self, export=True, display=False)
                    try:
                        list_png = sorted(
                            [str(pngfile) for pngfile in glob.glob(os.path.join(self.subdir, 'tmppng', '*.png'))])
                        # str(pngfile) to get non-unicode strings
                        clip = ImageSequenceClip(list_png, fps=min(self.tissue.f_sample, 25))

                        clip = clip.resize(400. / clip.size[0])

                        animation_name = os.path.join(self.subdir,
                                                      "myometrium_" + var + time.strftime("_%Y%m%d_%H%M%S", self.curr_time))
                        # clip.write_gif(animation_name+'.gif',fps=min(self.tissue.f_sample,25))
                        # self.graphfiles.append(os.path.split(animation_name)[1]+'.gif')
                        clip.write_videofile(animation_name + '.webm', codec='libvpx')
                        self.graphfiles.append(os.path.split(animation_name)[1] + '.webm')

                    except OSError as Err:
                        raise Err
                    else:
                        shutil.rmtree(os.path.join(self.subdir, 'tmppng'))


            if self.vc_mdl and _hasmoviepy:
                vidname = "skin_" + "potential" + time.strftime("_%Y%m%d_%H%M%S", self.curr_time) + '.mp4'
                self.graphfiles.append(graphics.vid_surf(self.vc.skinpot, name=vidname, manager=self))
            if self.electGrids:
                for id_grid, eg in enumerate(self.electGrids):
                    self.graphfiles.append(graphics.plot_grid(eg, id_grid, self))
                    self.graphfiles.append(graphics.plot_emg(eg, id_grid, self))


            report = reporting.TemplatedHTMLRepport(self)
            try:
                cssdir = os.path.join(self.subdir, '.css')
                if not (os.path.isdir(cssdir)):
                    os.mkdir(cssdir)
                shutil.copy(resource_filename(self.__module__.split('.')[0], 'data/css/normalize.css'), cssdir)
                shutil.copy(resource_filename(self.__module__.split('.')[0], 'data/css/skeleton.css'), cssdir)
                logger.info('Copied css files.')
            except OSError as Err:
                raise Err

            report.build()
        self.comptime['Outputs Generation'] = time.time() - t0
        logger.info("Computation time: " + repr(self.comptime))
        logger.handlers[0].stream.close()
        logger.removeHandler(logger.handlers[0])
