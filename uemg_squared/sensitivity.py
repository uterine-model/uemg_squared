# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import glob
import json
import os

import h5py
import numpy
import pysens
import uemg_squared
from PyQt4 import QtGui
import pyeeg
import copy
import traceback


def get_all(myjson, key):
    """
    Yeild all *key* in the input json
    :param myjson: Json input object
    :param key: key to return
    :return: dict
    """
    if type(myjson) is dict:
        for jsonkey in myjson:
            if jsonkey == key:
                yield myjson[jsonkey]
            elif type(myjson[jsonkey]) in (list, dict):
                for d in get_all(myjson[jsonkey], key):
                    yield d
    elif type(myjson) is list:
        for item in myjson:
            if type(item) in (list, dict):
                for d in get_all(item, key):
                    yield d


class EvalUEMGsq:
    def __init__(self, doe_csv, name_sim_json):
        self.evaluate = pysens.evaluate.Evaluator(doe_csv)
        self.evaluate.maxthreads = 8
        self.evaluate.run = self.run
        self.name_sim_json = name_sim_json
        self.base_json = None
        self.out_jsons = None
        self.jsons_to_run = []

    def doe2jsons(self):
        with open(self.name_sim_json) as fjson:
            self.base_json = json.load(fjson)

        self.out_jsons = dict()
        for line in self.evaluate.df_plan.T:
            self.out_jsons[line] = copy.deepcopy(self.base_json)
            for param in list(self.evaluate.df_plan.columns.get_level_values(0)):
                unit = self.evaluate.df_plan.iloc[line][param].axes[0].values[0]
                value = self.evaluate.df_plan.iloc[line][param].values[0]
                if unit.startswith('Unnamed'):
                    unit = ''
                if len(param.split('@')) == 1:
                    jpar = [p for parlist in get_all(self.out_jsons[line], 'parameters') for p in parlist if
                            p['name'] == param]
                    try:
                        jpar[0]['value'] = value
                        jpar[0]['unit'] = unit
                    except KeyError:
                        raise KeyError('Parameter {}'.format(param))
                    except IndexError:
                        raise IndexError('Model index {} for parameter {}.'.format(0, param))
                else:
                    par_name, m_idx = param.split('@')
                    jpar = [p for parlist in get_all(self.out_jsons[line], 'parameters') for p in parlist if
                            p['name'] == par_name]
                    try:
                        jpar[int(m_idx)]['value'] = value
                        jpar[int(m_idx)]['unit'] = unit
                    except KeyError:
                        raise KeyError('Parameter {}'.format(par_name))
                    except IndexError:
                        raise IndexError('Model index {} for parameter {}.'.format(m_idx, par_name))

        for idx, o_json in self.out_jsons.iteritems():
            out_name = os.path.join(self.evaluate.subdir, 'sensi_run_{:04}.json'.format(idx))
            with open(out_name, 'w') as out_file:
                json.dump(o_json, out_file, indent=4, sort_keys=True)
            self.jsons_to_run.append(out_name)

    def run(self, runidx):
        try:
            curr_json = self.jsons_to_run[runidx]
            m = uemg_squared.Manager(curr_json, basedir=self.evaluate.subdir, subdir='uemg_run_{0:04d}'.format(runidx))
            m.compute()
            m.finish(batch=True)
        except Exception:
            print("Exception in worker:")
            traceback.print_exc()
            raise


class ProcessUEMGsq(pysens.process.Processing):
    """
    Processing class for test models defined in Evaluator
    """
    def __init__(self, sigs_dir):
        """
        initialize based on specific function following parent scheme
        """
        # init feature functions dictionary
        self.feat_functions = {'mean': self.mean, 'samp_ent': self.samp_ent}#, 'dfa': self.dfa}#, 'lle':self.lle}

        # call parent init to do the actual processing
        pysens.process.Processing.__init__(self, sigs_dir)

    def load_signals(self):
        """
        Load signals saved in numpy arrays
        """
        for infile in sorted(glob.glob(os.path.join(self.path, '*/*.hdf5'))):
            try:
                with h5py.File(infile, "r") as f:
                    group=[grp for grp in f.keys() if grp.startswith('Grid')][0]
                    self.Sigs.append(numpy.array(f[group]['Monopolar']))
                # print infile
            except IOError:
                print ("Couldn't not load signals from file.")

    def mean(self, sig):
        """
        dummy feature extraction function which just returns the signal itself.
        :param sig:
        """
        return sig.mean()

    def samp_ent(self, sig):
            """
            dummy feature extraction function which just returns the signal itself.
            :param sig:
            """
            return numpy.mean([pyeeg.samp_entropy(s, 2, 0.2 * numpy.std(s)) for s in sig])

    #def dfa(self, sig):
    #        """
    #        dummy feature extraction function which just returns the signal itself.
    #        :param sig:
    #        """
    #        return numpy.mean([pyeeg.dfa(s) for s in sig])

    #def lle(self, sig):
    #        """
    #        dummy feature extraction function which just returns the signal itself.
    #        :param sig:
    #        """
    #        return numpy.mean([pyeeg.LLE() for s in sig])


def main():
    app = QtGui.QApplication([])
    sensi_json = str(QtGui.QFileDialog.getOpenFileName(None, "Open JSON Sensitivity description", "",
                                                       "JSON Files (*.json)"))
    base_json = str(QtGui.QFileDialog.getOpenFileName(None, "Open JSON Base Simulation description", "",
                                                      "JSON Files (*.json)"))

    sampl = pysens.sample.Morris(sensi_json)
    doe_csv = sampl.build()

    evalu = EvalUEMGsq(doe_csv, base_json)
    evalu.doe2jsons()

    if raw_input("Run simulations? (y/N)")=='y':

        evalu.evaluate.simulate()

        process = ProcessUEMGsq(evalu.evaluate.subdir)

        analysis = pysens.analyse.MorrisEE(doe_csv, os.path.join(evalu.evaluate.subdir,'Computed_features.csv'))
        analysis.analyse()
        analysis.plot_results()
        analysis.save_results()
    app.exit()

if __name__ == '__main__':
    main()
