# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import numpy
import scipy.fftpack
"""
Volume conduction for abdonimal tissues comes from:
C. Rabotti, M. Mischi, L. Beulen, S. G. Oei, and J. W. M. Bergmans,
“Modeling and identification of the electrohysterographic volume conductor by high-density electrodes,”
IEEE Trans. Biomed. Eng., vol. 57, pp. 519–527, 2010.
"""


class VolCond2D:
    """
    2D volume conductor for carthesian 2D surface
    """

    def __init__(self, param_spec):
        self.parameters = dict(sigma_abdox=0.4, sigma_abdoy=0.4, sigma_fat=0.04, sigma_myom=0.2, sigma_skin=0.5,
                               thick_abdo=9.36e-3, thick_fat=11.33e-3, thick_skin=2e-3)
        for p in param_spec.keys():
            self.parameters[p] = param_spec[p]
        for p in self.parameters.keys():
            self.__dict__[p] = self.parameters[p]
        if isinstance(self.shape, list):
            self.shape = tuple(self.shape)

        self.grid = numpy.meshgrid( # going from cm to m for cell sizes
            scipy.fftpack.fftshift(scipy.fftpack.fftfreq(int(self.shape[0]), self.cellsize[0]/100)),
            scipy.fftpack.fftshift(scipy.fftpack.fftfreq(int(self.shape[1]), self.cellsize[1]/100)))

        ratio_m_ax = self.sigma_myom / self.sigma_abdox
        ratio_ay_ax = self.sigma_abdoy / self.sigma_abdox
        ratio_f_ax = self.sigma_fat / self.sigma_abdox
        ratio_s_f = self.sigma_skin / self.sigma_fat

        z0 = 0.0
        ky = numpy.sqrt(self.grid[0] ** 2 + self.grid[1] ** 2)
        kya = numpy.sqrt(self.sigma_abdoy / self.sigma_abdox * (self.grid[0] ** 2 + self.grid[1] ** 2))

        alfa1 = ky * (
            ratio_m_ax * numpy.cosh(self.thick_abdo * kya) + numpy.sqrt(ratio_ay_ax) * numpy.sinh(
                self.thick_abdo * kya))
        alfa2 = ky * (
            ratio_m_ax * numpy.sinh(self.thick_abdo * kya) + numpy.sqrt(ratio_ay_ax) * numpy.cosh(
                self.thick_abdo * kya))
        self.spatial_filter = (2 / self.sigma_abdox) * (numpy.exp(ky * z0) * kya) * ((numpy.cosh(
            (self.thick_skin - self.thick_fat) * ky) * kya * alfa1 - ratio_f_ax * ky * numpy.sinh(
            (self.thick_skin - self.thick_fat) * ky) * alfa2) * (1 - ratio_s_f) + (numpy.cosh(
            (-self.thick_skin - self.thick_fat) * ky) * kya * alfa1 - ratio_f_ax * ky * numpy.sinh(
            (-self.thick_skin - self.thick_fat) * ky) * alfa2) * (1 + ratio_s_f)) ** (-1)
        self.spatial_filter[numpy.isnan(self.spatial_filter)] = 0.


        self.freq_vm = None
        self.freq_skinpot = None
        self.skinpot = None

    def compute(self, vm):
        self.freq_vm = scipy.fftpack.fftn(vm, axes=(1, 2))
        self.freq_skinpot = self.freq_vm * scipy.fftpack.fftshift(self.spatial_filter).reshape(1, self.shape[0], self.shape[1])
        self.skinpot = scipy.fftpack.ifftn(self.freq_skinpot, axes=(1, 2)).real


class VolCond3D:
    """
    2D volume conductor for carthesian 3D surface
    """

    def __init__(self, param_spec):
        self.parameters = dict(sigma_abdox=0.4, sigma_abdoy=0.4, sigma_fat=0.04, sigma_myom=0.2, sigma_skin=0.5,
                               thick_abdo=9.36e-3, thick_fat=11.33e-3, thick_skin=2e-3)
        for p in param_spec.keys():
            self.parameters[p] = param_spec[p]
        for p in self.parameters.keys():
            self.__dict__[p] = self.parameters[p]
        if isinstance(self.shape, list):
            self.shape = tuple(self.shape)

        self.grid = numpy.meshgrid(
            scipy.fftpack.fftshift(scipy.fftpack.fftfreq(self.shape[0], self.cellsize[0]/10)),
            scipy.fftpack.fftshift(scipy.fftpack.fftfreq(self.shape[1], self.cellsize[1]/10)))

        ratio_m_ax = self.sigma_myom / self.sigma_abdox
        ratio_ay_ax = self.sigma_abdoy / self.sigma_abdox
        ratio_f_ax = self.sigma_fat / self.sigma_abdox
        ratio_s_f = self.sigma_skin / self.sigma_fat

        rz0 = numpy.linspace(0., -self.cellsize[2]*self.shape[2], self.shape[2])
        ky = numpy.sqrt(self.grid[0] ** 2 + self.grid[1] ** 2)
        kya = numpy.sqrt(self.sigma_abdoy / self.sigma_abdox * (self.grid[0] ** 2 + self.grid[1] ** 2))
        self.spatial_filter = numpy.zeros(self.shape)
        for idz, z0 in enumerate(rz0):
            alfa1 = ky * (
                ratio_m_ax * numpy.cosh(self.thick_abdo * kya) + numpy.sqrt(ratio_ay_ax) * numpy.sinh(
                    self.thick_abdo * kya))
            alfa2 = ky * (
                ratio_m_ax * numpy.sinh(self.thick_abdo * kya) + numpy.sqrt(ratio_ay_ax) * numpy.cosh(
                    self.thick_abdo * kya))
            self.spatial_filter[...,idz] = (2 / self.sigma_abdox) * (numpy.exp(ky * z0) * kya) * ((numpy.cosh(
                (self.thick_skin - self.thick_fat) * ky) * kya * alfa1 - ratio_f_ax * ky * numpy.sinh(
                (self.thick_skin - self.thick_fat) * ky) * alfa2) * (1 - ratio_s_f) + (numpy.cosh(
                (-self.thick_skin - self.thick_fat) * ky) * kya * alfa1 - ratio_f_ax * ky * numpy.sinh(
                (-self.thick_skin - self.thick_fat) * ky) * alfa2) * (1 + ratio_s_f)) ** (-1)
        self.spatial_filter[numpy.isnan(self.spatial_filter)] = 0.


        self.freq_vm = None
        self.freq_skinpot = None
        self.skinpot = None

    def compute(self, vm):
        self.freq_vm = scipy.fftpack.fftn(vm, axes=(1, 2, 3))
        self.freq_skinpot = self.freq_vm * scipy.fftpack.fftshift(self.spatial_filter).reshape(1, self.shape[0], self.shape[1],self.shape[2])
        self.skinpot = scipy.fftpack.ifftn(self.freq_skinpot, axes=(1, 2, 3)).sum(axis=3).real
