# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import numpy


class Red3:
    """
    Cellular and tissular model *Red3*
    """

    def __init__(self, shape, param_spec):
        self.parameters = dict(Gk=0.064, Gkca=0.08, Gl=0.0055, Kd=0.01, fc=0.4, alpha=4 * 10 ** -5, Kca=0.01, El=-20,
                               Ek=-83,Gca2=0.02694061, vca2=-20.07451779, Rca=5.97139101, Jbase=0.02397327, T=295,
                               Ca0=3, Istim=0., capa_membrane=1)
        for p in param_spec.keys():
            self.parameters[p] = param_spec[p]
        for p in self.parameters.keys():
            self.__dict__[p] = self.parameters[p]
        self.Vm = -50. * numpy.ones(shape)
        self.nk = 0.079257 * numpy.ones(shape)
        self.Ca = 1e-4 * numpy.ones(shape)
        self.Istim = numpy.zeros(shape)
        self.time = 0.
        self.F=96.487
        self.R=8.314

    def derivT(self, dt):
        """
        Computes temporal derivative for *Red3* model with the Euler's method
        :param dt: integration time
        """
        # Nerst
        Eca = ((self.R * self.T) / (2 * self.F)) * numpy.log(self.Ca0 / self.Ca)
        # H inf x
        hki = 1 / (1 + numpy.exp((4.2 - self.Vm) / 21.1))
        # Tau x
        tnk = 23.75 * numpy.exp(-self.Vm / 72.15)
        # Courants
        Ica2 = self.Jbase + self.Gca2 * (self.Vm - Eca) / \
                            (1 + numpy.exp(-(self.Vm - self.vca2) / self.Rca))
        Ik = self.Gk * self.nk * (self.Vm - self.Ek)
        Ikca = self.Gkca * self.Ca ** 2 / (self.Ca ** 2 + self.Kd ** 2) * (self.Vm - self.Ek)
        Il = self.Gl * (self.Vm - self.El)
        # Derivees
        self.Vm += ((self.Istim - Ica2 - Ik - Ikca - Il) / self.capa_membrane) * dt
        self.nk += ((hki - self.nk) / tnk) * dt
        self.Ca += (self.fc * (-self.alpha * Ica2 - self.Kca * self.Ca)) * dt




class HaiMurphy:
    """
    [maggio2012modified]_ based on the model of [hai1988cross]_
    iniput parameters :
    Ca05MLCK: concentration of intracellular calcium that corresponds to the half activation of the myosin light chain kinasz
    nM: Hill coefficient of Ca05MLCK activation
    K2, K3, K4 and K7: transition rate from one state to another
    K: proportional coefficient of the force generation
    Available variables: K1, Force, HM_M, HM_Mp, HM_AMp, HM_AM
    """

    def __init__(self, shape, param_spec):
        """
        initialisation of Hai murphy's model parameter
        Ca05MLCK, nM, K1 à K7, Force et Y (for M, Mp, AMp et AM)
        """
        self.parameters = dict(Ca05MLCK=0.001, nM=4.7135, K2=0.1399 * 0.001, K3=14.4496 * 0.001, K4=3.6124 * 0.001,
                               K7=0.134 * 0.001, K=1)
        for p in param_spec:
            self.parameters[p] = param_spec[p]
        for p in self.parameters:
            self.__dict__[p] = self.parameters[p]

        self.time = 0.
        self.Force = numpy.zeros(shape)
        self.K1 = numpy.zeros(shape)
        self.HM_M = numpy.ones(shape)
        self.HM_Mp = numpy.zeros(shape)
        self.HM_AMp = numpy.zeros(shape)
        self.HM_AM = numpy.zeros(shape)

    def derivT(self, dt, Ca):
        """
        calculus of M Mp AMp et AM with the Euler's method
        :param dt: integration time
        :param Ca: calcium concentration from electrical model.
        """
        self.K1 = ((Ca ** self.nM) / (Ca ** self.nM + self.Ca05MLCK ** self.nM)) * 0.001
        self.HM_M = (- self.K1 * self.HM_M + self.K2 * self.HM_Mp + self.K7 * self.HM_AM) * dt + self.HM_M  # M
        self.HM_Mp = (self.K1 * self.HM_M - (self.K2 + self.K3) * self.HM_Mp + self.K4 * self.HM_AMp) * dt + self.HM_Mp
        # Mp
        self.HM_AMp = (self.K1 * self.HM_AM + self.K3 * self.HM_Mp - (
        self.K4 + self.K2) * self.HM_AMp) * dt + self.HM_AMp  # AMp
        self.HM_AM = (- (self.K1 + self.K7) * self.HM_AM + self.K2 * self.HM_AMp) * dt + self.HM_AM  # AM
        self.Force = self.K * (self.HM_AMp + self.HM_AM)
