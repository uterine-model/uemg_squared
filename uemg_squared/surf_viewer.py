# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import glob
import os
import shutil
import sys
from copy import deepcopy

import h5py
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import numpy
try:
    import pyqtgraph.opengl as gl
except ImportError:
    print("PyOpenGL is not installed!")
    raise
from PyQt4 import QtGui, QtCore
from moviepy.editor import CompositeVideoClip, ImageSequenceClip, TextClip
import moviepy.config as mvpconf
from pkg_resources import resource_filename


class SurfViewer(QtGui.QMainWindow):
    def __init__(self):
        super(SurfViewer, self).__init__()
        self.filename = None
        self.makingvideo = False
        self.centralWidget = QtGui.QWidget()
        self.setCentralWidget(self.centralWidget)
        self.lblHBOX = QtGui.QHBoxLayout()
        self.mainHBOX = QtGui.QHBoxLayout()
        self.mainVBOX = QtGui.QVBoxLayout()
        self.mainVBOX.addLayout(self.mainHBOX)
        self.mainVBOX.addLayout(self.lblHBOX)
        self.legends = []
        self.glviews = []
        self.centralWidget.setLayout(self.mainVBOX)
        self.utimer = QtCore.QTimer(self)
        self.utimer.timeout.connect(self.forward)
        exitAction = QtGui.QAction(QtGui.QIcon(resource_filename(self.__module__.split('.')[0], 'icons/exit.png')),
                                   'Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setToolTip('Exit application')
        exitAction.triggered.connect(self.close)

        loadAction = QtGui.QAction(QtGui.QIcon(resource_filename(self.__module__.split('.')[0], 'icons/open.png')),
                                   'Load', self)
        loadAction.setShortcut('Ctrl+O')
        loadAction.setToolTip('load application')
        loadAction.triggered.connect(self.load)

        self.AddViewAction = QtGui.QAction(
            QtGui.QIcon(resource_filename(self.__module__.split('.')[0], 'icons/add.png')), 'AddView', self)
        self.AddViewAction.setShortcut('Ctrl+P')
        self.AddViewAction.setToolTip('Add View')
        self.AddViewAction.triggered.connect(self.showAddView)
        self.AddViewAction.setEnabled(False)

        self.setScatterSizeAction = QtGui.QAction(
            QtGui.QIcon(resource_filename(self.__module__.split('.')[0], 'icons/dot.png')), 'setScatterSize', self)
        self.setScatterSizeAction.setToolTip('setScatterSize')
        self.setScatterSizeAction.triggered.connect(self.setScatterSize)
        self.setScatterSizeAction.setEnabled(False)

        self.ModViewAction = QtGui.QAction(
            QtGui.QIcon(resource_filename(self.__module__.split('.')[0], 'icons/edit.png')), 'ModView', self)
        self.ModViewAction.setShortcut('Ctrl+M')
        self.ModViewAction.setToolTip('Modify View')
        self.ModViewAction.triggered.connect(self.showModView)
        self.ModViewAction.setEnabled(False)

        self.RemViewAction = QtGui.QAction(
            QtGui.QIcon(resource_filename(self.__module__.split('.')[0], 'icons/remove.png')), 'RemView', self)
        self.RemViewAction.setToolTip('Remove View')
        self.RemViewAction.triggered.connect(self.remView)
        self.RemViewAction.setEnabled(False)

        self.playAction = QtGui.QAction(QtGui.QIcon(resource_filename(self.__module__.split('.')[0], 'icons/play.png')),
                                        'Play', self)
        self.playAction.setToolTip("Play simulation")
        self.playAction.triggered.connect(self.playsimu)
        self.playAction.setEnabled(False)

        self.pauseAction = QtGui.QAction(
            QtGui.QIcon(resource_filename(self.__module__.split('.')[0], 'icons/pause.png')), 'Pause', self)
        self.pauseAction.setToolTip("Pause simulation")
        self.pauseAction.triggered.connect(self.pausesimu)
        self.pauseAction.setEnabled(False)

        self.photoAction = QtGui.QAction(
            QtGui.QIcon(resource_filename(self.__module__.split('.')[0], 'icons/photo.png')), 'ScreenShot', self)
        self.photoAction.setToolTip("Screenshot simulation")
        self.photoAction.triggered.connect(self.screenshot)
        self.photoAction.setEnabled(False)

        self.videoAction = QtGui.QAction(
            QtGui.QIcon(resource_filename(self.__module__.split('.')[0], 'icons/video.png')), 'Video', self)
        self.videoAction.setToolTip("Export video")
        self.videoAction.triggered.connect(self.export_video)
        self.videoAction.setEnabled(False)

        aboutAction = QtGui.QAction('About', self)
        aboutAction.setToolTip("About qtviewer")
        aboutAction.triggered.connect(self.about)


        self.views = []
        menubar = self.menuBar()
        menubar.setNativeMenuBar(False)
        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(loadAction)
        fileMenu.addAction(exitAction)
        viewMenu = menubar.addMenu('&View')
        viewMenu.addAction(self.AddViewAction)
        viewMenu.addAction(self.ModViewAction)
        viewMenu.addAction(self.RemViewAction)
        # viewMenu.addAction(self.changecamview)
        simuMenu = menubar.addMenu('&Simulation')
        simuMenu.addAction(self.playAction)
        simuMenu.addAction(self.pauseAction)
        simuMenu.addAction(self.photoAction)
        simuMenu.addAction(self.videoAction)
        menubar.addAction(aboutAction)

        toolbar = self.addToolBar('Exit')
        self.combo_views = QtGui.QComboBox()
        self.combo_views.setEnabled(False)
        self.time_slider = QtGui.QSlider(QtCore.Qt.Horizontal)
        self.time_slider.setTickPosition(2)
        self.time_slider.setEnabled(False)
        self.time_slider.valueChanged[int].connect(self.updateTime)
        self.time_label = QtGui.QLabel("0.00 s ")
        toolbar.addAction(loadAction)
        toolbar.addAction(exitAction)
        toolbar.addSeparator()
        toolbar.addAction(self.AddViewAction)
        toolbar.addWidget(self.combo_views)
        toolbar.addAction(self.setScatterSizeAction)
        toolbar.addAction(self.ModViewAction)
        toolbar.addAction(self.RemViewAction)
        toolbar.addSeparator()
        toolbar.addAction(self.playAction)
        toolbar.addAction(self.pauseAction)
        toolbar.addWidget(self.time_slider)
        toolbar.addWidget(self.time_label)
        toolbar.addAction(self.photoAction)
        toolbar.addAction(self.videoAction)

        self.setWindowTitle('uEMQ QtViewer v0.1')
        self.pointsize = 1

    def setScatterSize(self):
        self.pointsize = QtGui.QInputDialog.getDouble(self, 'Set Scatter points size', 'Size', self.pointsize, 0.1, 10)[
            0]
        self.updateTime(self.time_slider.value())

    def about(self):
        QtGui.QMessageBox.about(self, "About QtViewer", "uEMG mesh simulation viewer.")

    def export_video(self):
        self.utimer.timeout.connect(self.autoscreen)
        self.subdir = os.path.join(self.workdir, "temp_images")
        self.makingvideo = True
        if os.path.isdir(self.subdir):
            reply = QtGui.QMessageBox.question(self, 'Temp directory',
                                               "Temporary directory already exist, erase it?", QtGui.QMessageBox.Yes |
                                               QtGui.QMessageBox.No, QtGui.QMessageBox.No)
            if reply == QtGui.QMessageBox.Yes:
                shutil.rmtree(self.subdir)
                os.mkdir(self.subdir)
        else:
            os.mkdir(self.subdir)
        self.utimer.start(1)

    def videoprocess(self):
        vidfile = QtGui.QFileDialog.getSaveFileName(self, "Save file", "", "GIF (*.gif);; Video (*.mp4 *.webm *.ogv)")
        if not vidfile.isEmpty():
            vidfile = str(vidfile)
            nbview = len(self.legends)
            clips = []
            fps = int(1000. / (self.time[1] - self.time[0]))
            imgs = ImageSequenceClip(sorted(glob.glob(os.path.join(str(self.subdir), str("m*.png")))), fps=fps)
            clips.append(imgs)
            positions_x = numpy.arange(0, imgs.size[0], imgs.size[0] / nbview) + imgs.size[0] / 10.
            if mvpconf.IMAGEMAGICK_BINARY != 'unset':
                for i, lbl in enumerate(self.legends):
                    txt = TextClip("\n".join(str(lbl.text()).split('_')), fontsize=24)
                    txt = txt.set_duration(imgs.duration)
                    txt = txt.set_position((positions_x[i], int(.9*imgs.size[1])))
                    clips.append(txt)
            else:
                print("Captions deactivated, please install ImageMagick.")
            video = CompositeVideoClip(clips)

            ext = os.path.splitext(vidfile)[-1]
            if ext == ".gif":
                video.write_gif(vidfile, fps=fps)
            else:
                video.write_videofile(vidfile, fps=fps)
            QtGui.QMessageBox.information(self, "Video export", "Video {} has been saved successfully.".format(vidfile))

        shutil.rmtree(self.subdir)
        self.makingvideo = False

    def autoscreen(self):
        self.screenshot(os.path.join(self.subdir, "mesh_{:04d}.png".format(self.time_slider.value())))

    def playsimu(self):
        self.utimer.stop()
        self.utimer.start(100)

    def pausesimu(self):
        self.utimer.stop()
        if self.makingvideo:
            self.videoprocess()
            self.utimer.timeout.disconnect(self.autoscreen)

    def screenshot(self, filename=None):
        if filename is None or filename is False:
            filename = QtGui.QFileDialog.getSaveFileName(self, "Save file", "", "*.png")
        img = QtGui.QPixmap(self.glviews[0].grabFrameBuffer())
        for nb_views in self.glviews[1:]:
            img2 = QtGui.QPixmap(nb_views.grabFrameBuffer())

            pixmap = QtGui.QPixmap(img.width() + img2.width(), img.height())

            left_rectF = QtCore.QRectF(0, 0, img.width(), img.height())  # the left half
            right_rectF = QtCore.QRectF(img.width(), 0, img2.width(), img.height())  # the right half

            painter = QtGui.QPainter(pixmap)
            painter.drawPixmap(left_rectF, img, QtCore.QRectF(img.rect()))
            painter.drawPixmap(right_rectF, img2, QtCore.QRectF(img2.rect()))
            painter.end()
            img = QtGui.QPixmap(pixmap)
        img.save(filename)

    def forward(self):
        val = self.time_slider.value()
        if val >= self.time_slider.maximum():
            self.utimer.stop()
            self.time_slider.setValue(0)
            if self.makingvideo:
                self.videoprocess()
                self.utimer.timeout.disconnect(self.autoscreen)
        else:
            self.time_slider.setValue(val + 1)

    def updateTime(self, value):
        self.time_label.setText("{:3.2f} s".format(self.time[value] / 1000.))
        for view in self.views:
            self.update_window(value, view)

    def update_window(self, nbi, viewsettings):
        if len(self.Vm.shape) == 2:
            print('error')
        elif len(self.Vm.shape) == 3:
            sp3 = viewsettings['GLItem']
            cmap = plt.get_cmap(viewsettings['cmap'])
            rgba_img = cmap(self.__dict__[viewsettings['var']][nbi].flatten())
            if viewsettings['deform'] == "Inactive":
                if viewsettings['mode'] == "ScatterPlot":
                    sp3.setData(pos=self.Points, color=rgba_img, size=self.pointsize)
                    sp3.update()
                else:
                    sp3.setMeshData(vertexes=self.Points, vertexColors=rgba_img, faces=self.Faces, smooth=False)
            elif viewsettings['deform'] == "Active":
                if viewsettings['mode'] == "ScatterPlot":
                    pos = numpy.array([self.Positions[nbi, 0].flatten() - self.Positions.shape[2] / 2,
                                       self.Positions[nbi, 1].flatten() - self.Positions.shape[3] / 2,
                                       numpy.zeros(self.Positions.shape[2] * self.Positions.shape[3])]).T
                    sp3.setData(pos=pos, color=rgba_img, size=self.pointsize)
                    sp3.update()
                else:
                    pos = numpy.array([self.Positions[nbi, 0].flatten() - self.Positions.shape[2] / 2,
                                       self.Positions[nbi, 1].flatten() - self.Positions.shape[3] / 2,
                                       numpy.zeros(self.Positions.shape[2] * self.Positions.shape[3])])
                    sp3.setMeshData(vertexes=pos.T, vertexColors=rgba_img, faces=self.Faces, smooth=False)

        elif len(self.Vm.shape) == 4:
            sp_h = viewsettings['3plan1']
            sp_v = viewsettings['3plan2']
            sp_p = viewsettings['3plan3']
            cmap = plt.get_cmap(viewsettings['cmap'])
            rgba_img_1, rgba_img_2, rgba_img_3 = self.f_plan_color(
                self.__dict__[viewsettings['var']][self.time_slider.value()], cmap=cmap)
            if viewsettings['deform'] == "Inactive":
                if viewsettings['mode'] == "ScatterPlot":
                    sp_v.setData(pos=self.Points_1, color=rgba_img_1, size=self.pointsize)
                    sp_h.setData(pos=self.Points_2, color=rgba_img_2, size=self.pointsize)
                    sp_p.setData(pos=self.Points_3, color=rgba_img_3, size=self.pointsize)
                    sp_v.update()
                    sp_h.update()
                    sp_p.update()
                else:
                    sp_v.setMeshData(vertexes=self.Points_1, vertexColors=rgba_img_1, faces=self.Faces_1, smooth=False)
                    sp_h.setMeshData(vertexes=self.Points_2, vertexColors=rgba_img_2, faces=self.Faces_2, smooth=False)
                    sp_p.setMeshData(vertexes=self.Points_3, vertexColors=rgba_img_3, faces=self.Faces_3, smooth=False)
                    sp_v.update()
                    sp_h.update()
                    sp_p.update()
            elif viewsettings['deform'] == "Active":
                self.Points_1, self.Points_2, self.Points_3 = self.f_plan_3d_coord(self.Positions[nbi])
                if viewsettings['mode'] == "ScatterPlot":
                    sp_v.setData(pos=self.Points_1, color=rgba_img_1, size=self.pointsize)
                    sp_h.setData(pos=self.Points_2, color=rgba_img_2, size=self.pointsize)
                    sp_p.setData(pos=self.Points_3, color=rgba_img_3, size=self.pointsize)
                    sp_v.update()
                    sp_h.update()
                    sp_p.update()
                else:
                    sp_v.setMeshData(vertexes=self.Points_1, vertexColors=rgba_img_1, faces=self.Faces_1, smooth=False)
                    sp_h.setMeshData(vertexes=self.Points_2, vertexColors=rgba_img_2, faces=self.Faces_2, smooth=False)
                    sp_p.setMeshData(vertexes=self.Points_3, vertexColors=rgba_img_3, faces=self.Faces_3, smooth=False)
                    sp_v.update()
                    sp_h.update()
                    sp_p.update()

    def f_plan_3d_coord(self, X, plan=None):
        if plan is None:
            plan = [X.shape[3] / 2, X.shape[2] / 2, X.shape[1] / 2]
        X_x = X[0, :, :, plan[0]]
        X_y = X[1, :, :, plan[0]]
        X_z = X[2, :, :, plan[0]]

        p1 = numpy.array([X_x.flatten() - plan[2],
                          X_y.flatten() - plan[1],
                          X_z.flatten() - plan[0]]).T

        X_x = X[0, :, plan[1], :]
        X_y = X[1, :, plan[1], :]
        X_z = X[2, :, plan[1], :]

        p2 = numpy.array([X_x.flatten() - plan[2],
                          X_y.flatten() - plan[1],
                          X_z.flatten() - plan[0]]).T

        X_x = X[0, plan[2], :, :]
        X_y = X[1, plan[2], :, :]
        X_z = X[2, plan[2], :, :]

        p3 = numpy.array([X_x.flatten() - plan[2],
                          X_y.flatten() - plan[1],
                          X_z.flatten() - plan[0]]).T
        return p1, p2, p3

    def off_reader(self, filename):

        nbpts, nbfaces, nbnormals = numpy.genfromtxt(filename, skip_header=1, max_rows=1, unpack=True, dtype=int)

        points = numpy.genfromtxt(filename, skip_header=2, max_rows=nbpts)
        rawfaces = numpy.genfromtxt(filename, skip_header=2 + nbpts, max_rows=nbfaces, dtype=int)
        if (rawfaces[:, 0] == 3).all():
            faces = rawfaces[:, 1:] - rawfaces[:, 1:].min()
        else:
            raise ValueError("Only triangular meshes are supported.")

        return points, faces

    def load(self):
        fname = str(QtGui.QFileDialog.getOpenFileName(self, 'Open HDF5 file',
                                                      '', "HDF5 (*.hdf5)"))
        if fname:
            self.workdir, self.filename = os.path.split(fname)
            with h5py.File(fname, "r") as f:
                self.time = numpy.array(f["/time"])
                if "myometrium" in f.keys():

                    self.Vm = numpy.array(f["/myometrium/Vm"])
                    self.Vm = (self.Vm - self.Vm.min()) / (self.Vm.max() - self.Vm.min())
                    try:
                        self.Force = numpy.array(f["/myometrium/Force"])
                        self.Force = (self.Force - self.Force.min()) / (self.Force.max() - self.Force.min())
                    except KeyError:
                        self.Force = None
                    try:
                        self.Stretch = numpy.array(f["/myometrium/Stretches"])
                        self.Stretch = self.Stretch / (2*abs(self.Stretch).max()) + 0.5
                    except KeyError:
                        self.Stretch = None
                    try:
                        self.Positions = numpy.array(f["/myometrium/Positions"])
                    except KeyError:
                        self.Positions = None
                    self.Blank = self.Vm * 0.
                    nb_pts = self.Vm.shape[1]
                    if len(self.Vm.shape) == 2:
                        print('error')
                    elif len(self.Vm.shape) == 3:
                        self.Faces, self.Points = self.f_plan_2d(self.Vm[0, ...])
                    elif len(self.Vm.shape) == 4:
                        self.Faces_1, self.Faces_2, self.Faces_3, self.Points_1, self.Points_2, self.Points_3 = \
                            self.f_plan_3d(
                            self.Vm[0, ...])

                self.time_slider.setRange(0, self.time.shape[0] - 1)
                self.time_slider.setEnabled(True)
                self.combo_views.setEnabled(True)
                self.AddViewAction.setEnabled(True)

    def f_plan_2d(self, X):
        index = numpy.arange((X.shape[0]) * (X.shape[1])).reshape(X.shape[0], X.shape[1])
        face_x = numpy.zeros(((X.shape[0] - 1) * (X.shape[1] - 1) * 2, 3), dtype=int)
        for l in range(1, X.shape[0]):
            for c in range(1, (X.shape[1] - 1) * 2 + 1):
                if numpy.mod(c - 1, 2) == 0:
                    face_x[(l - 1) * (X.shape[1] - 1) * 2 + (c - 1), 0] = index[l - 1, (c - 1) / 2]
                    face_x[(l - 1) * (X.shape[1] - 1) * 2 + (c - 1), 1] = index[l - 1, (c - 1) / 2 + 1]
                    face_x[(l - 1) * (X.shape[1] - 1) * 2 + (c - 1), 2] = index[l, (c - 1) / 2]
                if numpy.mod(c - 1, 2) == 1:
                    face_x[(l - 1) * (X.shape[1] - 1) * 2 + (c - 1), 0] = index[l, (c - 1) / 2]
                    face_x[(l - 1) * (X.shape[1] - 1) * 2 + (c - 1), 1] = index[l, (c - 1) / 2 + 1]
                    face_x[(l - 1) * (X.shape[1] - 1) * 2 + (c - 1), 2] = index[l - 1, (c - 1) / 2 + 1]

        lin = numpy.cumsum(numpy.ones(X.shape[0:2]), axis=0) - 1
        col = numpy.cumsum(numpy.ones(X.shape[0:2]), axis=1) - 1
        pos_x = numpy.array([lin.flatten() - lin.max() / 2., col.flatten() - col.max() / 2.,
                             numpy.zeros(X.shape[0:2]).flatten()]).transpose()
        return face_x, pos_x


    def f_plan_3d(self, X, plan=None):
        if plan is None:
            plan = [X.shape[2] / 2, X.shape[1] / 2, X.shape[0] / 2]
        X_x = X[:, :, plan[0]]
        X_y = X[:, plan[1], :]
        X_z = X[plan[2], :, :]
        index = numpy.arange((X_x.shape[0]) * (X_x.shape[1])).reshape(X_x.shape[0], X_x.shape[1])
        face_x = numpy.zeros(((X_x.shape[0] - 1) * (X_x.shape[1] - 1) * 2, 3), dtype=int)
        for l in range(1, X_x.shape[0]):
            for c in range(1, (X_x.shape[1] - 1) * 2 + 1):
                if numpy.mod(c - 1, 2) == 0:
                    face_x[(l - 1) * (X_x.shape[1] - 1) * 2 + (c - 1), 0] = index[l - 1, (c - 1) / 2]
                    face_x[(l - 1) * (X_x.shape[1] - 1) * 2 + (c - 1), 1] = index[l - 1, (c - 1) / 2 + 1]
                    face_x[(l - 1) * (X_x.shape[1] - 1) * 2 + (c - 1), 2] = index[l, (c - 1) / 2]
                if numpy.mod(c - 1, 2) == 1:
                    face_x[(l - 1) * (X_x.shape[1] - 1) * 2 + (c - 1), 0] = index[l, (c - 1) / 2]
                    face_x[(l - 1) * (X_x.shape[1] - 1) * 2 + (c - 1), 1] = index[l, (c - 1) / 2 + 1]
                    face_x[(l - 1) * (X_x.shape[1] - 1) * 2 + (c - 1), 2] = index[l - 1, (c - 1) / 2 + 1]

        lin = numpy.cumsum(numpy.ones(X.shape[0:2]), axis=0) - 1
        col = numpy.cumsum(numpy.ones(X.shape[0:2]), axis=1) - 1
        pos_x = numpy.array([lin.flatten() - lin.max() / 2., col.flatten() - col.max() / 2.,
                             numpy.zeros(X.shape[0:2]).flatten()]).transpose()

        index = numpy.arange((X_y.shape[0]) * (X_y.shape[1])).reshape(X_y.shape[0], X_y.shape[1])
        face_y = numpy.zeros(((X_y.shape[0] - 1) * (X_y.shape[1] - 1) * 2, 3), dtype=int)
        for l in range(1, X_y.shape[0]):
            for c in range(1, (X_y.shape[1] - 1) * 2 + 1):
                if numpy.mod(c - 1, 2) == 0:
                    face_y[(l - 1) * (X_y.shape[1] - 1) * 2 + (c - 1), 0] = index[l - 1, (c - 1) / 2]
                    face_y[(l - 1) * (X_y.shape[1] - 1) * 2 + (c - 1), 1] = index[l - 1, (c - 1) / 2 + 1]
                    face_y[(l - 1) * (X_y.shape[1] - 1) * 2 + (c - 1), 2] = index[l, (c - 1) / 2]
                if numpy.mod(c - 1, 2) == 1:
                    face_y[(l - 1) * (X_y.shape[1] - 1) * 2 + (c - 1), 0] = index[l, (c - 1) / 2]
                    face_y[(l - 1) * (X_y.shape[1] - 1) * 2 + (c - 1), 1] = index[l, (c - 1) / 2 + 1]
                    face_y[(l - 1) * (X_y.shape[1] - 1) * 2 + (c - 1), 2] = index[l - 1, (c - 1) / 2 + 1]

        col = numpy.cumsum(numpy.ones((X.shape[0], X.shape[2])), axis=0) - 1
        lin = numpy.cumsum(numpy.ones((X.shape[0], X.shape[2])), axis=1) - 1
        pos_y = numpy.array([col.flatten() - col.max() / 2., numpy.zeros((X.shape[0], X.shape[2])).flatten(),
                             lin.flatten() - lin.max() / 2.]).transpose()

        index = numpy.arange((X_z.shape[0]) * (X_z.shape[1])).reshape(X_z.shape[0], X_z.shape[1])
        face_z = numpy.zeros(((X_z.shape[0] - 1) * (X_z.shape[1] - 1) * 2, 3), dtype=int)
        for l in range(1, X_z.shape[0]):
            for c in range(1, (X_z.shape[1] - 1) * 2 + 1):
                if numpy.mod(c - 1, 2) == 0:
                    face_z[(l - 1) * (X_z.shape[1] - 1) * 2 + (c - 1), 0] = index[l - 1, (c - 1) / 2]
                    face_z[(l - 1) * (X_z.shape[1] - 1) * 2 + (c - 1), 1] = index[l - 1, (c - 1) / 2 + 1]
                    face_z[(l - 1) * (X_z.shape[1] - 1) * 2 + (c - 1), 2] = index[l, (c - 1) / 2]
                if numpy.mod(c - 1, 2) == 1:
                    face_z[(l - 1) * (X_z.shape[1] - 1) * 2 + (c - 1), 0] = index[l, (c - 1) / 2]
                    face_z[(l - 1) * (X_z.shape[1] - 1) * 2 + (c - 1), 1] = index[l, (c - 1) / 2 + 1]
                    face_z[(l - 1) * (X_z.shape[1] - 1) * 2 + (c - 1), 2] = index[l - 1, (c - 1) / 2 + 1]

        col = numpy.cumsum(numpy.ones(X.shape[1:3]), axis=0) - 1
        lin = numpy.cumsum(numpy.ones(X.shape[1:3]), axis=1) - 1
        pos_z = numpy.array([numpy.zeros(X.shape[1:3]).flatten(), col.flatten() - col.max() / 2.,
                             lin.flatten() - lin.max() / 2.]).transpose()

        return face_x, face_y, face_z, pos_x, pos_y, pos_z

    def showAddView(self):
        if self.filename is not None:
            if len(self.views) < 4:
                settings = AddViewDialog(self)
                settings.id = None
                settings.show()
            else:
                QtGui.QMessageBox.warning(self, "Views", "Views are already complete.")
        else:
            QtGui.QMessageBox.warning(self, "Views", "Please load signals first.")

    def showModView(self):
        if len(self.views) > 0:
            settings = AddViewDialog(self)
            settings.id = self.combo_views.currentIndex()
            settings.set_settings()
            settings.show()
        else:
            QtGui.QMessageBox.warning(self, "Views", "No views defined yet.")

    def remView(self):
        id = self.combo_views.currentIndex()
        maxid = self.combo_views.count()
        for modid in range(id + 1, maxid):
            self.combo_views.setItemText(modid, "View {}".format(modid))
        self.mainHBOX.removeWidget(self.glviews[id])
        self.glviews[id].deleteLater()
        self.glviews.pop(id)
        self.views.pop(id)
        self.lblHBOX.removeWidget(self.legends[id])
        self.legends[id].deleteLater()
        self.legends.pop(id)
        self.combo_views.removeItem(id)
        if len(self.views)==0:
            self.setScatterSizeAction.setEnabled(False)
            self.ModViewAction.setEnabled(False)
            self.RemViewAction.setEnabled(False)
            self.playAction.setEnabled(False)
            self.pauseAction.setEnabled(False)
            self.photoAction.setEnabled(False)
            self.videoAction.setEnabled(False)

    def addGLView(self, viewsettings):
        if len(self.Vm.shape) == 2:
            print('error')
        elif len(self.Vm.shape) == 3:
            w = gl.GLViewWidget(self.centralWidget)
            w.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
            w.setBackgroundColor('w')
            w.opts['distance'] = 2 * numpy.max(self.Points)
            w.opts['fov'] = 60
            w.opts['azimuth'] = 90
            w.opts['elevation'] = 90  # -10
            cmap = plt.get_cmap(viewsettings['cmap'])
            rgba_img = cmap(self.__dict__[viewsettings['var']][self.time_slider.value()].flatten())
            if viewsettings['mode'] == "ScatterPlot":
                sp3 = gl.GLScatterPlotItem(pos=self.Points, color=rgba_img, size=self.pointsize, pxMode=False)
                sp3.setGLOptions('translucent')
            else:
                sp3 = gl.GLMeshItem(vertexes=self.Points, vertexColors=rgba_img, faces=self.Faces, smooth=False)
            viewsettings['GLItem'] = sp3
            w.addItem(sp3)

        elif len(self.Vm.shape) == 4:
            w = gl.GLViewWidget(self.centralWidget)
            w.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
            w.setBackgroundColor('w')
            w.opts['distance'] = 2 * numpy.max((self.Points_1.max(), self.Points_2.max(), self.Points_3.max()))
            w.opts['fov'] = 60
            w.opts['azimuth'] = 90
            w.opts['elevation'] = 90  # -10
            cmap = plt.get_cmap(viewsettings['cmap'])
            rgba_img_1, rgba_img_2, rgba_img_3 = self.f_plan_color(
                self.__dict__[viewsettings['var']][self.time_slider.value()], cmap=cmap)
            if viewsettings['mode'] == "ScatterPlot":
                sp_v = gl.GLScatterPlotItem(pos=self.Points_1, color=rgba_img_1, size=self.pointsize, pxMode=False)
                sp_v.setGLOptions('translucent')
                sp_h = gl.GLScatterPlotItem(pos=self.Points_2, color=rgba_img_2, size=self.pointsize, pxMode=False)
                sp_h.setGLOptions('translucent')
                sp_p = gl.GLScatterPlotItem(pos=self.Points_3, color=rgba_img_3, size=self.pointsize, pxMode=False)
                sp_p.setGLOptions('translucent')
            else:
                sp_v = gl.GLMeshItem(vertexes=self.Points_1, vertexColors=rgba_img_1, faces=self.Faces_1, smooth=False)
                sp_h = gl.GLMeshItem(vertexes=self.Points_2, vertexColors=rgba_img_2, faces=self.Faces_2, smooth=False)
                sp_p = gl.GLMeshItem(vertexes=self.Points_3, vertexColors=rgba_img_3, faces=self.Faces_3, smooth=False)

            viewsettings['3plan1'] = sp_v
            viewsettings['3plan2'] = sp_h
            viewsettings['3plan3'] = sp_p

            w.addItem(sp_v)
            w.addItem(sp_h)
            w.addItem(sp_p)


        return w

    def f_plan_color(self, X, plan=None, cmap=None):
        if plan is None:
            plan = [X.shape[2] / 2, X.shape[1] / 2, X.shape[0] / 2]
        X_x = X[:, :, plan[0]]
        X_y = X[:, plan[1], :]
        X_z = X[plan[2], :, :]
        color_x = cmap(X_x.flatten())
        color_y = cmap(X_y.flatten())
        color_z = cmap(X_z.flatten())
        return color_x, color_y, color_z


class AddViewDialog(QtGui.QDialog):
    def __init__(self, parent=None):
        super(AddViewDialog, self).__init__(parent)
        self.parent = parent
        self.setupUi(self)

    def setupUi(self, Dialog):
        Dialog.setWindowModality(QtCore.Qt.ApplicationModal)
        Dialog.setWindowTitle("Adding view {}".format(len(self.parent.views) + 1))
        self.buttonBox = QtGui.QDialogButtonBox(Dialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel | QtGui.QDialogButtonBox.Ok)
        self.formLayoutWidget = QtGui.QWidget(Dialog)
        self.formLayout = QtGui.QFormLayout(self.formLayoutWidget)
        self.formLayout.setFieldGrowthPolicy(QtGui.QFormLayout.AllNonFixedFieldsGrow)

        self.label_file = QtGui.QLabel("Current dataset: {}".format(str(self.parent.filename)))
        self.formLayout.addWidget(self.label_file)
        self.label_var = QtGui.QLabel("Variable:", self.formLayoutWidget)
        varlayout = QtGui.QHBoxLayout()
        self.var_group = QtGui.QButtonGroup(self)
        self.radio01 = QtGui.QRadioButton("Vm")
        self.var_group.addButton(self.radio01)
        varlayout.addWidget(self.radio01)
        self.radio02 = QtGui.QRadioButton("Force")
        if self.parent.Force is None:
            self.radio02.setEnabled(False)
        self.var_group.addButton(self.radio02)
        varlayout.addWidget(self.radio02)
        self.radio03 = QtGui.QRadioButton("Stretch")
        self.var_group.addButton(self.radio03)
        varlayout.addWidget(self.radio03)
        if self.parent.Stretch is None:
            self.radio03.setEnabled(False)
        self.radio04 = QtGui.QRadioButton("Blank")
        self.var_group.addButton(self.radio04)
        varlayout.addWidget(self.radio04)
        self.radio01.setChecked(True)
        self.formLayout.addRow(self.label_var, varlayout)

        self.label_mode = QtGui.QLabel("Mode:", self.formLayoutWidget)
        modelayout = QtGui.QHBoxLayout()
        self.mode_group = QtGui.QButtonGroup(self)
        self.radio11 = QtGui.QRadioButton("ScatterPlot")
        self.mode_group.addButton(self.radio11)
        modelayout.addWidget(self.radio11)
        self.radio12 = QtGui.QRadioButton("Textured")
        self.mode_group.addButton(self.radio12)
        modelayout.addWidget(self.radio12)
        self.radio11.setChecked(True)
        self.formLayout.addRow(self.label_mode, modelayout)

        self.label_deform = QtGui.QLabel("Deformations:", self.formLayoutWidget)
        deformlayout = QtGui.QHBoxLayout()
        self.deform_group = QtGui.QButtonGroup(self)
        self.radio21 = QtGui.QRadioButton("Active")
        self.deform_group.addButton(self.radio21)
        deformlayout.addWidget(self.radio21)
        if self.parent.Positions is None:
            self.radio21.setEnabled(False)
        self.radio22 = QtGui.QRadioButton("Inactive")
        self.deform_group.addButton(self.radio22)
        deformlayout.addWidget(self.radio22)
        self.radio22.setChecked(True)
        self.formLayout.addRow(self.label_deform, deformlayout)

        self.label_cmap = QtGui.QLabel("Colormap:")
        self.combo_cmap = QtGui.QComboBox()
        self.combo_cmap.addItems(sorted(cm.cmap_d.keys()))
        default_cmap = self.combo_cmap.findText('viridis', QtCore.Qt.MatchFixedString)
        if default_cmap >= 0:
            self.combo_cmap.setCurrentIndex(default_cmap)
        else:
            self.combo_cmap.setCurrentIndex(self.combo_cmap.findText('jet', QtCore.Qt.MatchFixedString))
        self.formLayout.addRow(self.label_cmap, self.combo_cmap)

        self.formLayout.addWidget(self.buttonBox)
        Dialog.setLayout(self.formLayout)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("accepted()"), self.valid_settings)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("rejected()"), Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def set_settings(self):
        [bt.setChecked(True) for bt in self.var_group.buttons() if str(bt.text()) == self.parent.views[self.id]['var']]
        [bt.setChecked(True) for bt in self.mode_group.buttons() if
         str(bt.text()) == self.parent.views[self.id]['mode']]
        [bt.setChecked(True) for bt in self.deform_group.buttons() if
         str(bt.text()) == self.parent.views[self.id]['deform']]
        self.combo_cmap.setCurrentIndex(
            self.combo_cmap.findText(self.parent.views[self.id]['cmap'], QtCore.Qt.MatchFixedString))

    def valid_settings(self):

        self.view_settings = dict()
        self.view_settings['var'] = str(self.var_group.checkedButton().text())
        self.view_settings['mode'] = str(self.mode_group.checkedButton().text())
        self.view_settings['deform'] = str(self.deform_group.checkedButton().text())
        self.view_settings['cmap'] = str(self.combo_cmap.currentText())
        lbldict1 = dict(Vm="Electrical", Force="Mechanical", Stretch="Local Stretch", Blank="")
        lbldict2 = dict(Active="_With Deformations", Inactive="")
        if self.id is None:
            self.parent.views.append(self.view_settings)
            self.parent.combo_views.addItem("View {}".format(len(self.parent.views)))
            w = self.parent.addGLView(self.view_settings)
            lbl = QtGui.QLabel(
                "{}{}".format(lbldict1[self.view_settings['var']], lbldict2[self.view_settings['deform']]))
            lbl.setAlignment(QtCore.Qt.AlignCenter)
            self.parent.legends.append(lbl)
            self.parent.glviews.append(w)
            self.parent.mainHBOX.addWidget(w)
            self.parent.lblHBOX.addWidget(lbl)
            self.parent.setScatterSizeAction.setEnabled(True)
            self.parent.ModViewAction.setEnabled(True)
            self.parent.RemViewAction.setEnabled(True)
            self.parent.playAction.setEnabled(True)
            self.parent.pauseAction.setEnabled(True)
            self.parent.photoAction.setEnabled(True)
            self.parent.videoAction.setEnabled(True)
        else:
            self.parent.views[self.id] = self.view_settings
            self.parent.mainHBOX.removeWidget(self.parent.glviews[self.id])
            self.parent.glviews[self.id].deleteLater()
            self.parent.glviews[self.id] = self.parent.addGLView(self.view_settings)
            self.parent.mainHBOX.insertWidget(self.id, self.parent.glviews[self.id])
        self.accept()


    def miseajour(self):
        self.view_settings = dict()
        self.view_settings['mode'] = str(self.mode_group.checkedButton().text())
        self.view_settings['deform'] = str(self.deform_group.checkedButton().text())
        self.view_settings['cmap'] = str(self.combo_cmap.currentText())
        print(self.parent.views)
        nbview = len(self.parent.glviews)
        for View in range(nbview):
            self.view_settings['var'] = self.parent.views[View]['var']
            self.parent.views[View] = deepcopy((self.view_settings))
            self.parent.mainHBOX.removeWidget(self.parent.glviews[View])
            self.parent.glviews[View].deleteLater()
            self.parent.glviews[View] = self.parent.addGLView(self.parent.views[View])
            self.parent.mainHBOX.insertWidget(View, self.parent.glviews[View])

        self.accept()




def main():
    app = QtGui.QApplication(sys.argv)
    ex = SurfViewer()
    ex.showMaximized()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
