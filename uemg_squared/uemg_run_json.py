# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from PyQt4 import QtGui
import uemg_squared
import os
import sys


def main():
    app = QtGui.QApplication(sys.argv)
    fjson = str(QtGui.QFileDialog.getOpenFileName(None, "Open JSON Simulation description", "", "JSON Files (*.json)"))
    uemg = uemg_squared.Manager(fjson, os.path.split(fjson)[0])
    uemg.compute()
    uemg.finish(batch=False)


if __name__ == '__main__':
    main()