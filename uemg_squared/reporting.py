from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
import getpass
import os
import platform
import time
import logging
import psutil
import h5py
import glob
from collections import OrderedDict
from jinja2 import Environment, FileSystemLoader
from pkg_resources import resource_filename

try:
    import cpuinfo
except ImportError:
    _hascpuinfo = False
else:
    _hascpuinfo = True

logger = logging.getLogger(__name__)


def clean_content(content):
    to_pop = []
    for key, val in content.items():
        paths = key.split('/')
        if len(paths) == 2:
            content[paths[0]][paths[-1]] = val
            to_pop.append(key)
        elif len(paths) == 3:
            content[paths[0]][paths[1]][paths[-1]] = val
            to_pop.append(key)
    for key in to_pop:
        content.pop(key)


def prettify(d, indent=0):
    '''
    Print the file tree structure with proper indentation.
    '''
    text = ""
    for key, value in d.items():
        if isinstance(value, dict):
            if value['type'] == 'group':
                text += "[{}]\n".format(key)
                text += prettify(value, indent + 1)
            else:
                text += '\t' * indent + "{} {}\n".format(key, value['shape'])
        else:
            pass
    return text


def hdf5_read(h5file):
    content = dict()

    def dict_app(key, val):
        val_list = repr(val).split(' ')
        val_type = val_list[1]
        if val_type == 'dataset':
            val_shape = ' '.join(val_list[val_list.index('shape') + 1:val_list.index('type')])[:-1]
            content[key] = {'type': val_type, 'shape': val_shape}
        else:
            content[key] = {'type': val_type}

    with h5py.File(h5file, 'r') as f:
        f.visititems(dict_app)

    clean_content(content)
    return prettify(content)


class HTMLReport:
    """
    Generate html reports of simulations.
    """

    def __init__(self, manager):
        self.manager = manager
        self.header = ""
        self.body = ""
        self.footer = ""

    def build(self):

        if self.manager.username is None:
            self.manager.username = getpass.getuser()
        self.header = """<!DOCTYPE html><html><head>
          <link rel=\"stylesheet\" href=\".css/normalize.css\">
          <link rel=\"stylesheet\" href=\".css/skeleton.css\">
          </head><body>
          """
        self.body = ""
        self.footer = "</body></html>"
        self.body += "<div class=\"container\"><h1>Simulation Report</h1><h5>General informations</h5>"
        self.body += """<table>
        <tr><td>Model</td><td>{mdl}</td></tr>
        <tr><td>Model version</td><td>{version}</td></tr>
        <tr><td>User</td><td>{user}</td></tr>
        <tr><td>Machine name</td><td>{machine}</td></tr>
        <tr><td>Time</td><td>{time}</td></tr>
        </table>
        """.format(mdl=str(self.manager.__module__), version=self.manager.code_version, user=self.manager.username,
                   machine=platform.node(),
                   time=time.strftime("%Hh%M:%S %d/%m/%Y", self.manager.curr_time), )

        self.body += "<strong>Purpose:</strong><p>{}</p>".format(self.manager.purpose)

        self.body += "<strong>Files in {}:</strong>".format(self.manager.subdir)
        self.body += "<p>" + ", ".join(
            ["<a href=\"{file}\">{file}</a> ".format(file=f) for f in sorted(os.listdir(self.manager.subdir)) if
             not f.startswith('.css')]) + "</p>"
        self.body += "</div>"

        self.body += "<div class=\"container\"><strong>HDF5 Results content:</strong>"
        self.h5content = hdf5_read(glob.glob(os.path.join(self.manager.subdir, '*.hdf5'))[0])
        self.body += "<pre><code>" + self.h5content + "</code></pre>"
        self.body += "</div>"

        self.body += "<div class=\"container\"><h5>Graphics generated</h5><div class=\"row\">"
        for img in self.manager.graphfiles:
            self.body += "<figure class=\"three columns\">"
            if os.path.split(img)[1].split('.')[1] in ['png', 'jpg', 'gif', 'svg']:
                self.body += "<img src={im} alt=\"{im}\" ><p>{im}</p>".format(im=os.path.split(img)[1])
            elif os.path.split(img)[1].split('.')[1] in ['mp4', 'webm']:
                self.body += """<video width="250" controls autoplay loop>
                          <source src="{}" type="video/{}">
                        Your browser does not support the video tag, File {} not rendered.
                        </video>""".format(img, os.path.split(img)[1].split('.')[1], os.path.split(img)[1])
            else:
                self.body += "<p>Unrendered Multimedia file: {}</p>".format(os.path.split(img)[1])
            self.body += "<figcaption>{}</figcaption></figure>".format(
                ' '.join([l for l in os.path.split(img)[1].split('.')[0].split('_') if l.isalpha()]))
        # self.story.append(ImageFigure(self.manager.subdir + img + '.png', str(img)))
        #    self.story.append(Spacer(cm, 1 * cm))
        self.body += "</div></div>"
        self.body += "<div class=\"container\"><h5>Computation times</h5>"
        self.body += "<table>"
        self.body += "".join(
            ["<tr><td>{}</td><td>{:.3f}s</td></tr>\n".format(k, v) for k, v in self.manager.comptime.items()])
        self.body += "</table>"
        self.body += "</div>"
        self.body += "<div class=\"container\">"
        self.body += "<h5>Hardware informations</h5>"
        sysname = platform.system()

        if sysname == "Linux":
            osname = ' '.join(platform.linux_distribution())
            with open('/proc/cpuinfo', 'r') as f:
                cpuname = [line for line in f.readlines() if 'model name' in line][0].split(':')[1].strip('\n')
        elif sysname == "Windows":
            osname = platform.win32_ver()[0]
            cpuname = platform.processor()
        elif sysname == "Darwin":
            sysname == "MacOS X"
            osname = platform.mac_ver()[0]
            cpuname = platform.processor()
        if _hascpuinfo:
            cpuname = cpuinfo.get_cpu_info()['brand']
        self.body += "<table>"
        self.body += "<tr><td>Operating System:</td><td>" + sysname + " {}</td></tr>".format(osname)
        self.body += "<tr><td>Cpu configuration:</td><td>{}</td></tr>".format(cpuname)
        self.body += "<tr><td>Total numbers:</td><td>{} cores, {} threads</td></tr>".format(psutil.cpu_count(False),
                                                                                            psutil.cpu_count())
        self.body += "<tr><td>Total amount of memory:</td><td>{:.2g}GB</td></tr>".format(
            psutil.virtual_memory()[0] / (1024 ** 3))
        self.body += "</table>"
        self.body += "</div>"

        self.body += "<div class=\"container\"><h5>Parameters</h5><ul>"
        for mdl in self.manager.params_calc:
            if str(mdl.__class__).startswith("<"):
                mname = str(mdl.__class__).split('\'')[1]
            else:
                mname = mdl.__class__
            self.body += "<li><strong>{}</strong>".format(mname)
            self.body += "<table>"
            self.body += "<thead><tr><th>Name</th><th>Value</th><th>Unit</th></tr></thead>"
            self.body += "".join(
                ["<tr><td>{}</td><td>{}</td><td>{}</td></tr>\n".format(k, v, self.manager.units[k]) for k, v in
                 sorted(mdl.parameters.items())])
            self.body += "</table></li>"
        self.body += '</ul></div>'
        doc = self.header + self.body + self.footer
        with open(os.path.join(self.manager.subdir, 'report.html'), 'w') as fhtml:
            fhtml.write(doc)
        logger.info("Wrote html report.")


class TemplatedHTMLRepport:
    def __init__(self, manager):
        self.manager = manager

    def build(self):
        if self.manager.username is None:
            self.manager.username = getpass.getuser()
        env = Environment(loader=FileSystemLoader(os.path.join(resource_filename(self.__module__.split('.')[0], 'data'))))
        template = env.get_template('report_tmpl.html')

        general = OrderedDict()
        general['Model'] = str(self.manager.__module__)
        general['Model Version'] = self.manager.code_version
        general['User'] = self.manager.username
        general['Machine name'] = platform.node()
        general['Time'] = time.strftime("%Hh%M:%S %d/%m/%Y", self.manager.curr_time)
        purpose = self.manager.purpose

        files = sorted(filter(lambda cfile: not cfile.startswith('.'), os.listdir(self.manager.subdir)))

        dgraphs = sorted(
            [dict(path=im, name=os.path.split(im)[1], ext=os.path.split(im)[1].split('.')[1]) for im in
             self.manager.graphfiles],
            key=lambda x: x['ext'])

        sysname = platform.system()

        if sysname == "Linux":
            osname = ' '.join(platform.linux_distribution())
            with open('/proc/cpuinfo', 'r') as f:
                cpuname = [line for line in f.readlines() if 'model name' in line][0].split(':')[1].strip('\n')
        elif sysname == "Windows":
            osname = platform.win32_ver()[0]
            cpuname = platform.processor()
        elif sysname == "Darwin":
            sysname = "MacOS X"
            osname = platform.mac_ver()[0]
            cpuname = platform.processor()
        else:
            osname = ""
            cpuname = platform.processor()

        hard = OrderedDict()
        hard['Operating System:'] = " ".join([sysname, osname])
        hard['Cpu configuration:'] = cpuname
        hard['Total numbers:'] = "{} cores, {} threads".format(psutil.cpu_count(False), psutil.cpu_count())
        hard['Total amount of memory:'] = "{:.2f}GB".format(psutil.virtual_memory()[0] / (1024 ** 3))

        try:
            h5content = hdf5_read(glob.glob(os.path.join(self.manager.subdir, '*.hdf5'))[0])
        except IndexError:
            h5content = ""

        params = list()
        for mdl in self.manager.params_calc:
            if str(mdl.__class__).startswith('<'):
                mname = str(mdl.__class__).split('\'')[1]
            else:
                mname = str(mdl.__class__)

            mparams = [dict(name=k, value=v, unit=self.manager.units[k]) for k, v in sorted(mdl.parameters.items())]
            params.append({'class': mname, 'parameters': mparams})

        with open(os.path.join(self.manager.subdir, 'report.html'), 'w') as fhtml:
            fhtml.write(
                template.render(gen=general, pur=purpose, dir=self.manager.subdir, files=files, h5cont=h5content,
                                graphfiles=dgraphs, comptime=self.manager.comptime, hard=hard,
                                params_calc=params))
        logger.info("Wrote html report.")
