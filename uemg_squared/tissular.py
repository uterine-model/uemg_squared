# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import logging

import numpy

try:
    from progressbar import Bar, Percentage, ProgressBar, ETA

    _hasprogress = True
except ImportError:
    _hasprogress = False
from scipy.ndimage.filters import convolve

logger = logging.getLogger(__name__)


class Tissue(object):
    """Generic tissue model."""

    def __init__(self, param_spec):
        self.params = {}
        self.Vm = None
        self.tsave = None
        self.mdl = None
        self.Iamp = 0.2
        self.mdl_elec = None
        self.mdl_force = None


class TissueGrid(Tissue):
    """
    Cartesian grid tissue model.
    Provides electrical and force models.
    """

    def __init__(self, param_spec):
        """
        Contructor of Tissue grid.
        :param param_spec: dictionary specifying parameters values
        """
        self.parameters = dict(dt=0.1, tmax=5e3, f_sample=20, shape=(10, 10), capa_membrane=1,
                               cellsize_x=0.05, cellsize_y=0.05, cellsize_z=0.05, resist_x=2000, resist_y=2000,
                               resist_z=2000,
                               stim_list=[])
        for p in param_spec.keys():
            self.parameters[p] = param_spec[p]
        for p in self.parameters.keys():
            self.__dict__[p] = self.parameters[p]
        if isinstance(self.shape, list):
            self.shape = tuple(self.shape)
        if len(self.shape) == 1:
            self.deriv_s = self.diff1dc
            self.convmask = numpy.array([1., -2., 1]) / (self.resist_x * self.capa_membrane * self.cellsize_x ** 2)
        elif len(self.shape) == 2:
            self.deriv_s = self.diff2dc
            convmask = numpy.zeros((3, 3))
            convmask[1, [0, 2]] = 1 / (self.resist_x * self.capa_membrane * self.cellsize_x ** 2)
            convmask[[0, 2], 1] = 1 / (self.resist_y * self.capa_membrane * self.cellsize_y ** 2)
            convmask[1, 1] = -convmask.sum()
            self.convmask = convmask
        elif len(self.shape) == 3:
            self.deriv_s = self.diff3dc
            convmask = numpy.zeros((3, 3, 3))
            convmask[1, 1, [0, 2]] = 1 / (self.resist_x * self.capa_membrane * self.cellsize_x ** 2)
            convmask[1, [0, 2], 1] = 1 / (self.resist_y * self.capa_membrane * self.cellsize_y ** 2)
            convmask[[0, 2], 1, 1] = 1 / (self.resist_z * self.capa_membrane * self.cellsize_z ** 2)
            convmask[1, 1, 1] = -convmask.sum()
            self.convmask = convmask
        else:
            self.deriv_s = lambda x: 0

        super(TissueGrid, self).__init__(param_spec)

    def apply_stim(self, stim_list):
        """
        Applies all stimulations to the electrical model at the current time.
        :param stim_list: list of stim dictionaries
        """
        self.mdl_elec.Istim = numpy.zeros_like(self.mdl_elec.Istim)
        for stim in stim_list:
            window = 1 / (1 + numpy.exp(-self.mdl_elec.time + stim['delay'])) - 1 / (
                1 + numpy.exp(-self.mdl_elec.time + stim['delay'] + stim['duration']))
            pulse = 2 * numpy.pi * (
                self.mdl_elec.time / (2.0 * stim['duration']) - stim['delay'] / (2.0 * stim['duration']))
            amp_stim = stim['amp'] * window * numpy.sin(pulse)
            if self.mdl_elec.Istim.shape == (1, 1):
                self.mdl_elec.Istim += amp_stim
            else:
                idx_stim = [range(l - s, l + s) for (l, s) in zip(stim['loc'], stim['size'])]
                self.mdl_elec.Istim[numpy.meshgrid(*idx_stim)] += amp_stim

    def __repr__(self):
        """Print model infos."""
        return "Model {}, dimensions: {}, electrical model: {}, force model: {}.".format(self.__class__, self.shape,
                                                                                         self.mdl_elec.__class__,
                                                                                         self.mdl_force.__class__)

    def diff1dc(self, var):
        """Computes spatial derivative to get diffusion.
        :param var: 1d ndarray to convolve with self.convmask
        :rtype: ndarray
        """
        return convolve(var, self.convmask, mode='mirror')

    def diff2dc(self, var):
        """Computes spatial derivative to get diffusion.
        :param var: 2d ndarray to convolve with self.convmask
        :rtype: ndarray
        """
        return convolve(var, self.convmask, mode='mirror')

    def diff3dc(self, var):
        """Computes spatial derivative to get diffusion.
        :param var: 3d ndarray to convolve with self.convmask
        :rtype: ndarray
        """
        return convolve(var, self.convmask, mode='mirror')

    def compute(self):
        """
        Integrates the models over time.

        """

        self.tsave = numpy.arange(0, self.tmax, 1 / (self.f_sample * 1e-3))
        self.Vm = numpy.empty(tuple([len(self.tsave)]) + self.mdl_elec.Vm.shape)
        if self.mdl_force:
            self.Force = numpy.empty_like(self.Vm)
        # Integration
        if _hasprogress:
            pbar = ProgressBar(widgets=[Percentage(), Bar(), ETA()], maxval=self.tmax).start()
        for i, ti in enumerate(self.tsave):

            while self.mdl_elec.time < ti:
                self.apply_stim(self.stim_list)
                self.mdl_elec.derivT(self.dt)
                self.mdl_elec.Vm += self.deriv_s(self.mdl_elec.Vm) * self.dt * (self.mdl_elec.Istim == 0)
                self.mdl_elec.time += self.dt
            if self.mdl_force:
                self.mdl_force.derivT(ti - self.mdl_force.time, self.mdl_elec.Ca)
                self.mdl_force.time = ti
                self.Force[i] = self.mdl_force.Force.copy()
            self.Vm[i] = self.mdl_elec.Vm.copy()
            if _hasprogress:
                pbar.update(ti)
        if _hasprogress:
            pbar.finish()


class TissueGridDeformation(TissueGrid):
    """
    Cartesian grid tissue model.
    Provides electrical, force and *deformation* models.
    """

    def __init__(self, param_spec):
        """
        Contructor of Tissue grid.
        :param param_spec: dictionary specifying parameters values
        """
        TissueGrid.__init__(self, param_spec)
        self.s_E = 1
        self.s_n = 1
        if len(self.shape) == 1:
            self.deformation_cartesien = self._deformation_cartesien_1d
            self.stretch_x = numpy.zeros((self.shape[0] - 1))
            self.R_x = numpy.zeros((self.shape[0] - 1))
            self.D_x = numpy.zeros((self.shape[0]))
        elif len(self.shape) == 2:
            self.deformation_cartesien = self._deformation_cartesien_2d
            self.stretch_x = numpy.zeros((self.shape[0] - 1, self.shape[1]))
            self.R_x = numpy.zeros((self.shape[0] - 1, self.shape[1]))
            self.D_x = numpy.zeros((self.shape[0], self.shape[1]))
            self.stretch_y = numpy.zeros((self.shape[0], self.shape[1] - 1))
            self.R_y = numpy.zeros((self.shape[0], self.shape[1] - 1))
            self.D_y = numpy.zeros((self.shape[0], self.shape[1]))

        elif len(self.shape) == 3:
            self.deformation_cartesien = self._deformation_cartesien_3d
            self.stretch_x = numpy.zeros((self.shape[0] - 1, self.shape[1], self.shape[2]))
            self.R_x = numpy.zeros((self.shape[0] - 1, self.shape[1], self.shape[2]))
            self.D_x = numpy.zeros((self.shape[0], self.shape[1], self.shape[2]))
            self.stretch_y = numpy.zeros((self.shape[0], self.shape[1] - 1, self.shape[2]))
            self.R_y = numpy.zeros((self.shape[0], self.shape[1] - 1, self.shape[2]))
            self.D_y = numpy.zeros((self.shape[0], self.shape[1], self.shape[2]))
            self.stretch_z = numpy.zeros((self.shape[0], self.shape[1], self.shape[2] - 1))
            self.R_z = numpy.zeros((self.shape[0], self.shape[1], self.shape[2] - 1))
            self.D_z = numpy.zeros((self.shape[0], self.shape[1], self.shape[2]))

    def compute(self):
        """
        Integrates the models over time.
        """

        self.tsave = numpy.arange(0, self.tmax, 1 / (self.f_sample * 1e-3))
        self.Vm = numpy.empty(self.tsave.shape + self.mdl_elec.Vm.shape)
        self.Positions = numpy.empty(self.tsave.shape + (len(self.shape),) + self.mdl_elec.Vm.shape)
        self.Stretches = numpy.empty(self.tsave.shape + self.mdl_elec.Vm.shape)
        self.Var_stret = sorted([s for s in self.__dict__.keys() if s.startswith('stretch')])
        if self.mdl_force:
            self.Force = numpy.empty_like(self.Vm)
        # Integration
        if _hasprogress:
            pbar = ProgressBar(widgets=[Percentage(), Bar(), ETA()], maxval=self.tmax).start()
        for i, ti in enumerate(self.tsave):
            while self.mdl_elec.time < ti:
                self.apply_stim(self.stim_list)
                self.mdl_elec.derivT(self.dt)
                self.mdl_elec.Vm += self.deriv_s(self.mdl_elec.Vm) * self.dt * (self.mdl_elec.Istim == 0)
                self.mdl_elec.time += self.dt
            # stores time and state
            self.mdl_force.derivT(ti - self.mdl_force.time, self.mdl_elec.Ca)
            self.mdl_force.time = ti
            self.Stretches[i] = self.global_stretch()
            self.Force[i] = self.mdl_force.Force.copy()
            self.reset_stretch()
            self.Positions[i] = self.deformation_cartesien()
            self.Vm[i] = self.mdl_elec.Vm.copy()
            if _hasprogress:
                pbar.update(ti)
        if _hasprogress:
            pbar.finish()

    def reset_stretch(self):
        """
        Reset the stretches to zeros.
        """
        for s in self.Var_stret:
            self.__dict__[s] *= 0.

    def global_stretch(self):
        """
        Generate a global stretch matrix
        :return: Stretch matrix
        """
        global_stretch = numpy.zeros_like(self.mdl_elec.Vm)
        tmp_stretch = numpy.zeros_like(self.mdl_elec.Vm)
        for axis, s in enumerate(self.Var_stret):
            axis_stretch = self.__dict__[s]
            if axis == 0:
                tmp_stretch[:-1] = axis_stretch
                tmp_stretch[1:] += axis_stretch
                tmp_stretch[1:-1] /= 2.
            elif axis == 1:
                tmp_stretch[:, :-1] = axis_stretch
                tmp_stretch[:, 1:] += axis_stretch
                tmp_stretch[:, 1:-1] /= 2.
            elif axis == 2:
                tmp_stretch[..., :-1] = axis_stretch
                tmp_stretch[..., 1:] += axis_stretch
                tmp_stretch[..., 1:-1] /= 2.

            global_stretch += tmp_stretch

        return global_stretch/len(self.Var_stret)


    def _deformation_cartesien_1d(self):
        """Computes spatial deformation. (1D)
        :rtype: ndarray
        """
        for l in range(self.mdl_force.Force.shape[0] - 1):
            self.R_x[l] = (self.mdl_force.Force[l] + self.mdl_force.Force[l + 1]) / 2 - numpy.mean(self.mdl_force.Force,
                                                                                                   0)
            self.stretch_x[l] += ((self.R_x[l] - self.s_E * self.stretch_x[l]) / self.s_n) * self.dt
            self.D_x[l + 1] = 1 - self.stretch_x[l, :]
        self.D_x = numpy.cumsum(self.D_x)
        return self.D_x

    def _deformation_cartesien_2d(self):
        """Computes spatial deformation. (2D)
        :rtype: tuple of ndarray
        """
        for l in range(self.mdl_force.Force.shape[0] - 1):
            self.R_x[l, :] = (self.mdl_force.Force[l, :] + self.mdl_force.Force[l + 1, :]) / 2 - numpy.mean(
                self.mdl_force.Force, 0)
            self.stretch_x[l, :] = ((self.R_x[l, :] - self.s_E * self.stretch_x[l,
                                                                 :]) / self.s_n) * self.dt + self.stretch_x[l, :]
            self.D_x[l + 1, :] = 1 - self.stretch_x[l, :]
        self.D_x = numpy.cumsum(self.D_x, 0)
        for c in range(self.mdl_force.Force.shape[1] - 1):
            self.R_y[:, c] = (self.mdl_force.Force[:, c] + self.mdl_force.Force[:, c + 1]) / 2 - numpy.mean(
                self.mdl_force.Force, 1)
            self.stretch_y[:, c] = ((self.R_y[:, c] - self.s_E * self.stretch_y[:,
                                                                 c]) / self.s_n) * self.dt + self.stretch_y[:, c]
            self.D_y[:, c + 1] = 1 - self.stretch_y[:, c]
        self.D_y = numpy.cumsum(self.D_y, 1)
        return self.D_x, self.D_y

    def _deformation_cartesien_3d(self):
        """Computes spatial deformation. (3D)
        :rtype: tuple of ndarray
        """
        k = 10
        for l in range(self.mdl_force.Force.shape[0] - 1):
            self.R_x[l, :, :] = (self.mdl_force.Force[l, :, :] * k + self.mdl_force.Force[l + 1, :,
                                                                     :] * k) / 2 - numpy.mean(self.mdl_force.Force * k,
                                                                                              0)
            self.stretch_x[l, :, :] = ((self.R_x[l, :, :] - self.s_E * self.stretch_x[l, :,
                                                                       :]) / self.s_n) * self.dt + self.stretch_x[l, :,
                                                                                                   :]
            self.D_x[l + 1, :, :] = 1 - self.stretch_x[l, :, :]
        self.D_x = numpy.cumsum(self.D_x, 0)
        for c in range(self.mdl_force.Force.shape[1] - 1):
            self.R_y[:, c, :] = (self.mdl_force.Force[:, c, :] * k + self.mdl_force.Force[:, c + 1,
                                                                     :] * k) / 2 - numpy.mean(self.mdl_force.Force * k,
                                                                                              1)
            self.stretch_y[:, c, :] = ((self.R_y[:, c, :] - self.s_E * self.stretch_y[:, c,
                                                                       :]) / self.s_n) * self.dt + self.stretch_y[:, c,
                                                                                                   :]
            self.D_y[:, c, :] = 1 - self.stretch_y[:, c, :]
        self.D_y = numpy.cumsum(self.D_y, 1)
        for p in range(self.mdl_force.Force.shape[2] - 1):
            self.R_z[:, :, p] = (self.mdl_force.Force[:, :, p] * k + self.mdl_force.Force[:, :,
                                                                     p + 1] * k) / 2 - numpy.mean(
                self.mdl_force.Force * k, 2)
            self.stretch_z[:, :, p] = ((self.R_z[:, :, p] - self.s_E * self.stretch_z[:, :,
                                                                       p]) / self.s_n) * self.dt + self.stretch_z[:, :,
                                                                                                   p]
            self.D_z[:, :, p + 1] = 1 - self.stretch_z[:, :, p]
        self.D_z = numpy.cumsum(self.D_z, 2)
        return self.D_x, self.D_y, self.D_z

