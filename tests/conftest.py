# -*- coding: utf-8 -*-

import pytest
import shutil

@pytest.fixture(scope='module')
def init_model():
    import uemg_squared

    m = uemg_squared.Manager("../uemg_squared/data/test_uemg.json")
    m.compute()
    yield m
    print("Cleaning up test run.")
    shutil.rmtree(m.subdir)


@pytest.fixture(scope='module')
def loaded_results():
    import h5py
    import numpy
    results = dict()

    with h5py.File('Results_test_uemg.hdf5','r') as rh5:
        results['Time'] = numpy.array(rh5['time'])
        results['Vm'] = numpy.array(rh5['/myometrium/Vm'])
        results['Force'] = numpy.array(rh5['/myometrium/Force'])
        results['Skin'] = numpy.array(rh5['/Skin/Potential'])
        results['Signals'] = numpy.array(rh5['/EMG/Grid_1/Monopolar'])

    return results