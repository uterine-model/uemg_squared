# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import pytest
import numpy


def test_timings(loaded_results, init_model):
    assert numpy.allclose(loaded_results['Time'], init_model.tissue.tsave)


def test_vm(loaded_results, init_model):
    assert numpy.allclose(loaded_results['Vm'], init_model.tissue.Vm)


def test_force(loaded_results, init_model):
    assert numpy.allclose(loaded_results['Force'], init_model.tissue.Force)


def test_skinpot(loaded_results, init_model):
    assert numpy.allclose(loaded_results['Skin'], init_model.vc.skinpot)


def test_emg(loaded_results, init_model):
    assert numpy.allclose(loaded_results['Signals'], init_model.electGrids[0].sigs)

