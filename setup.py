from setuptools import setup, find_packages

setup(name='uemg_squared',
      version='0.1',
      description='Uterine ElectroMyoGraphy Model on cartesian grid.',
      url='https://gitlab.utc.fr/jlaforet/uemg-squared',
      author='Jeremy Laforet',
      author_email='jlaforet@utc.fr',
      license='GPLv3',
      packages=find_packages(),
      entry_points={'gui_scripts': ["uemg_viewer = uemg_squared.surf_viewer:main"],'console_scripts': [
            "uemg_runner = uemg_squared.uemg_run_json:main"]},
      package_data={'uemg_squared': ['data/*.json','data/*.html', 'data/css/*.css', 'icons/*.png']},
      install_requires=['h5py', 'astropy', 'jsonschema', 'psutil', 'scipy', 'numpy', 'moviepy', 'pyqtgraph',
                        'matplotlib', 'pytest'],
      extra_require={'progress': 'progressbar'}

      )
